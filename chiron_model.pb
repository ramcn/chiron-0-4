
>
PlaceholderPlaceholder*
dtype0*
shape:
��
;
Placeholder_1Placeholder*
dtype0*
shape:�
8
Placeholder_2Placeholder*
dtype0
*
shape:
J
Reshape/shapeConst*%
valueB"L     ,     *
dtype0
E
ReshapeReshapePlaceholderReshape/shape*
T0*
Tshape0
�
Cres_layer1/branch1/conv1/weights/Initializer/truncated_normal/shapeConst*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*%
valueB"            *
dtype0
�
Bres_layer1/branch1/conv1/weights/Initializer/truncated_normal/meanConst*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*
valueB
 *    *
dtype0
�
Dres_layer1/branch1/conv1/weights/Initializer/truncated_normal/stddevConst*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*
valueB
 *���=*
dtype0
�
Mres_layer1/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalCres_layer1/branch1/conv1/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
Ares_layer1/branch1/conv1/weights/Initializer/truncated_normal/mulMulMres_layer1/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalDres_layer1/branch1/conv1/weights/Initializer/truncated_normal/stddev*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
=res_layer1/branch1/conv1/weights/Initializer/truncated_normalAddAres_layer1/branch1/conv1/weights/Initializer/truncated_normal/mulBres_layer1/branch1/conv1/weights/Initializer/truncated_normal/mean*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
 res_layer1/branch1/conv1/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
'res_layer1/branch1/conv1/weights/AssignAssign res_layer1/branch1/conv1/weights=res_layer1/branch1/conv1/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*
validate_shape(
�
%res_layer1/branch1/conv1/weights/readIdentity res_layer1/branch1/conv1/weights"/device:CPU:0*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
res_layer1/branch1/conv1/conv1Conv2DReshape%res_layer1/branch1/conv1/weights/read*
paddingSAME*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
|
Cres_layer1/branch1/conv1_bn/conv1_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
1res_layer1/branch1/conv1_bn/conv1_bn_moments/meanMeanres_layer1/branch1/conv1/conv1Cres_layer1/branch1/conv1_bn/conv1_bn_moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
9res_layer1/branch1/conv1_bn/conv1_bn_moments/StopGradientStopGradient1res_layer1/branch1/conv1_bn/conv1_bn_moments/mean*
T0
�
>res_layer1/branch1/conv1_bn/conv1_bn_moments/SquaredDifferenceSquaredDifferenceres_layer1/branch1/conv1/conv19res_layer1/branch1/conv1_bn/conv1_bn_moments/StopGradient*
T0
�
Gres_layer1/branch1/conv1_bn/conv1_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
5res_layer1/branch1/conv1_bn/conv1_bn_moments/varianceMean>res_layer1/branch1/conv1_bn/conv1_bn_moments/SquaredDifferenceGres_layer1/branch1/conv1_bn/conv1_bn_moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
4res_layer1/branch1/conv1_bn/conv1_bn_moments/SqueezeSqueeze1res_layer1/branch1/conv1_bn/conv1_bn_moments/mean*
squeeze_dims
 *
T0
�
6res_layer1/branch1/conv1_bn/conv1_bn_moments/Squeeze_1Squeeze5res_layer1/branch1/conv1_bn/conv1_bn_moments/variance*
squeeze_dims
 *
T0
�
Mres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/shapeConst*
dtype0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
valueB:�
�
Lres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/meanConst*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
valueB
 *    *
dtype0
�
Nres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/stddevConst*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
valueB
 *�d�=*
dtype0
�
Wres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalMres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/shape*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
dtype0*
seed2 *

seed 
�
Kres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/mulMulWres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/TruncatedNormalNres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/stddev*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale
�
Gres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normalAddKres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/mulLres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal/mean*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale
�
*res_layer1/branch1/conv1_bn/conv1_bn_scale
VariableV2"/device:CPU:0*
shape:�*
shared_name *=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
dtype0*
	container 
�
1res_layer1/branch1/conv1_bn/conv1_bn_scale/AssignAssign*res_layer1/branch1/conv1_bn/conv1_bn_scaleGres_layer1/branch1/conv1_bn/conv1_bn_scale/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
validate_shape(
�
/res_layer1/branch1/conv1_bn/conv1_bn_scale/readIdentity*res_layer1/branch1/conv1_bn/conv1_bn_scale"/device:CPU:0*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale
�
Nres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/shapeConst*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
valueB:�*
dtype0
�
Mres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/meanConst*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
valueB
 *    *
dtype0
�
Ores_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/stddevConst*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
valueB
 *�d�=*
dtype0
�
Xres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalNres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/shape*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
dtype0*
seed2 *

seed 
�
Lres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/mulMulXres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/TruncatedNormalOres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/stddev*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset
�
Hres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normalAddLres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/mulMres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal/mean*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset
�
+res_layer1/branch1/conv1_bn/conv1_bn_offset
VariableV2"/device:CPU:0*
shape:�*
shared_name *>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
dtype0*
	container 
�
2res_layer1/branch1/conv1_bn/conv1_bn_offset/AssignAssign+res_layer1/branch1/conv1_bn/conv1_bn_offsetHres_layer1/branch1/conv1_bn/conv1_bn_offset/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
validate_shape(
�
0res_layer1/branch1/conv1_bn/conv1_bn_offset/readIdentity+res_layer1/branch1/conv1_bn/conv1_bn_offset"/device:CPU:0*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset
X
+res_layer1/branch1/conv1_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
)res_layer1/branch1/conv1_bn/batchnorm/addAdd6res_layer1/branch1/conv1_bn/conv1_bn_moments/Squeeze_1+res_layer1/branch1/conv1_bn/batchnorm/add/y*
T0
h
+res_layer1/branch1/conv1_bn/batchnorm/RsqrtRsqrt)res_layer1/branch1/conv1_bn/batchnorm/add*
T0
�
)res_layer1/branch1/conv1_bn/batchnorm/mulMul+res_layer1/branch1/conv1_bn/batchnorm/Rsqrt/res_layer1/branch1/conv1_bn/conv1_bn_scale/read*
T0
�
+res_layer1/branch1/conv1_bn/batchnorm/mul_1Mulres_layer1/branch1/conv1/conv1)res_layer1/branch1/conv1_bn/batchnorm/mul*
T0
�
+res_layer1/branch1/conv1_bn/batchnorm/mul_2Mul4res_layer1/branch1/conv1_bn/conv1_bn_moments/Squeeze)res_layer1/branch1/conv1_bn/batchnorm/mul*
T0
�
)res_layer1/branch1/conv1_bn/batchnorm/subSub0res_layer1/branch1/conv1_bn/conv1_bn_offset/read+res_layer1/branch1/conv1_bn/batchnorm/mul_2*
T0
�
+res_layer1/branch1/conv1_bn/batchnorm/add_1Add+res_layer1/branch1/conv1_bn/batchnorm/mul_1)res_layer1/branch1/conv1_bn/batchnorm/sub*
T0
�
Dres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*%
valueB"            *
dtype0
�
Cres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
valueB
 *    *
dtype0
�
Eres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/stddevConst*
dtype0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
valueB
 *���=
�
Nres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/shape*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
dtype0*
seed2 *

seed 
�
Bres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/mulMulNres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalEres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights
�
>res_layer1/branch2/conv2a/weights/Initializer/truncated_normalAddBres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/mulCres_layer1/branch2/conv2a/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights
�
!res_layer1/branch2/conv2a/weights
VariableV2"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
dtype0*
	container *
shape:�*
shared_name 
�
(res_layer1/branch2/conv2a/weights/AssignAssign!res_layer1/branch2/conv2a/weights>res_layer1/branch2/conv2a/weights/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights
�
&res_layer1/branch2/conv2a/weights/readIdentity!res_layer1/branch2/conv2a/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights
�
 res_layer1/branch2/conv2a/conv2aConv2DReshape&res_layer1/branch2/conv2a/weights/read*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME*
	dilations
*
T0
~
Eres_layer1/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indicesConst*
dtype0*!
valueB"          
�
3res_layer1/branch2/conv2a_bn/conv2a_bn_moments/meanMean res_layer1/branch2/conv2a/conv2aEres_layer1/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
;res_layer1/branch2/conv2a_bn/conv2a_bn_moments/StopGradientStopGradient3res_layer1/branch2/conv2a_bn/conv2a_bn_moments/mean*
T0
�
@res_layer1/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceSquaredDifference res_layer1/branch2/conv2a/conv2a;res_layer1/branch2/conv2a_bn/conv2a_bn_moments/StopGradient*
T0
�
Ires_layer1/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indicesConst*
dtype0*!
valueB"          
�
7res_layer1/branch2/conv2a_bn/conv2a_bn_moments/varianceMean@res_layer1/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceIres_layer1/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
6res_layer1/branch2/conv2a_bn/conv2a_bn_moments/SqueezeSqueeze3res_layer1/branch2/conv2a_bn/conv2a_bn_moments/mean*
squeeze_dims
 *
T0
�
8res_layer1/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1Squeeze7res_layer1/branch2/conv2a_bn/conv2a_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shapeConst*
dtype0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
valueB:�
�
Nres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddevConst*
dtype0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *�d�=
�
Yres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulMulYres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale
�
Ires_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normalAddMres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulNres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale
�
,res_layer1/branch2/conv2a_bn/conv2a_bn_scale
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale
�
3res_layer1/branch2/conv2a_bn/conv2a_bn_scale/AssignAssign,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleIres_layer1/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal"/device:CPU:0*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(*
use_locking(
�
1res_layer1/branch2/conv2a_bn/conv2a_bn_scale/readIdentity,res_layer1/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale
�
Pres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
valueB:�*
dtype0
�
Ores_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shape*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
dtype0*
seed2 *

seed 
�
Nres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulMulZres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
�
Jres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normalAddNres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulOres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
�
-res_layer1/branch2/conv2a_bn/conv2a_bn_offset
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
�
4res_layer1/branch2/conv2a_bn/conv2a_bn_offset/AssignAssign-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetJres_layer1/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(*
use_locking(
�
2res_layer1/branch2/conv2a_bn/conv2a_bn_offset/readIdentity-res_layer1/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
Y
,res_layer1/branch2/conv2a_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer1/branch2/conv2a_bn/batchnorm/addAdd8res_layer1/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1,res_layer1/branch2/conv2a_bn/batchnorm/add/y*
T0
j
,res_layer1/branch2/conv2a_bn/batchnorm/RsqrtRsqrt*res_layer1/branch2/conv2a_bn/batchnorm/add*
T0
�
*res_layer1/branch2/conv2a_bn/batchnorm/mulMul,res_layer1/branch2/conv2a_bn/batchnorm/Rsqrt1res_layer1/branch2/conv2a_bn/conv2a_bn_scale/read*
T0
�
,res_layer1/branch2/conv2a_bn/batchnorm/mul_1Mul res_layer1/branch2/conv2a/conv2a*res_layer1/branch2/conv2a_bn/batchnorm/mul*
T0
�
,res_layer1/branch2/conv2a_bn/batchnorm/mul_2Mul6res_layer1/branch2/conv2a_bn/conv2a_bn_moments/Squeeze*res_layer1/branch2/conv2a_bn/batchnorm/mul*
T0
�
*res_layer1/branch2/conv2a_bn/batchnorm/subSub2res_layer1/branch2/conv2a_bn/conv2a_bn_offset/read,res_layer1/branch2/conv2a_bn/batchnorm/mul_2*
T0
�
,res_layer1/branch2/conv2a_bn/batchnorm/add_1Add,res_layer1/branch2/conv2a_bn/batchnorm/mul_1*res_layer1/branch2/conv2a_bn/batchnorm/sub*
T0
b
#res_layer1/branch2/conv2a_relu/reluRelu,res_layer1/branch2/conv2a_bn/batchnorm/add_1*
T0
�
Dres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*%
valueB"            *
dtype0
�
Cres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
valueB
 *    *
dtype0
�
Eres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
valueB
 *�(=*
dtype0
�
Nres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/shape*

seed *
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
dtype0*
seed2 
�
Bres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/mulMulNres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalEres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights
�
>res_layer1/branch2/conv2b/weights/Initializer/truncated_normalAddBres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/mulCres_layer1/branch2/conv2b/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights
�
!res_layer1/branch2/conv2b/weights
VariableV2"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
dtype0*
	container *
shape:��*
shared_name 
�
(res_layer1/branch2/conv2b/weights/AssignAssign!res_layer1/branch2/conv2b/weights>res_layer1/branch2/conv2b/weights/Initializer/truncated_normal"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
validate_shape(*
use_locking(
�
&res_layer1/branch2/conv2b/weights/readIdentity!res_layer1/branch2/conv2b/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights
�
 res_layer1/branch2/conv2b/conv2bConv2D#res_layer1/branch2/conv2a_relu/relu&res_layer1/branch2/conv2b/weights/read*
paddingSAME*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
~
Eres_layer1/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer1/branch2/conv2b_bn/conv2b_bn_moments/meanMean res_layer1/branch2/conv2b/conv2bEres_layer1/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
;res_layer1/branch2/conv2b_bn/conv2b_bn_moments/StopGradientStopGradient3res_layer1/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0
�
@res_layer1/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceSquaredDifference res_layer1/branch2/conv2b/conv2b;res_layer1/branch2/conv2b_bn/conv2b_bn_moments/StopGradient*
T0
�
Ires_layer1/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer1/branch2/conv2b_bn/conv2b_bn_moments/varianceMean@res_layer1/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceIres_layer1/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer1/branch2/conv2b_bn/conv2b_bn_moments/SqueezeSqueeze3res_layer1/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0*
squeeze_dims
 
�
8res_layer1/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1Squeeze7res_layer1/branch2/conv2b_bn/conv2b_bn_moments/variance*
T0*
squeeze_dims
 
�
Ores_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
valueB:�*
dtype0
�
Nres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulMulYres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
Ires_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normalAddMres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulNres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
,res_layer1/branch2/conv2b_bn/conv2b_bn_scale
VariableV2"/device:CPU:0*
shape:�*
shared_name *?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
	container 
�
3res_layer1/branch2/conv2b_bn/conv2b_bn_scale/AssignAssign,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleIres_layer1/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
1res_layer1/branch2/conv2b_bn/conv2b_bn_scale/readIdentity,res_layer1/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
Pres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
valueB:�*
dtype0
�
Ores_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shape*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
dtype0*
seed2 *

seed 
�
Nres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulMulZres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset
�
Jres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normalAddNres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulOres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset
�
-res_layer1/branch2/conv2b_bn/conv2b_bn_offset
VariableV2"/device:CPU:0*
shape:�*
shared_name *@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
dtype0*
	container 
�
4res_layer1/branch2/conv2b_bn/conv2b_bn_offset/AssignAssign-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetJres_layer1/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(*
use_locking(
�
2res_layer1/branch2/conv2b_bn/conv2b_bn_offset/readIdentity-res_layer1/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset
Y
,res_layer1/branch2/conv2b_bn/batchnorm/add/yConst*
dtype0*
valueB
 *��'7
�
*res_layer1/branch2/conv2b_bn/batchnorm/addAdd8res_layer1/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1,res_layer1/branch2/conv2b_bn/batchnorm/add/y*
T0
j
,res_layer1/branch2/conv2b_bn/batchnorm/RsqrtRsqrt*res_layer1/branch2/conv2b_bn/batchnorm/add*
T0
�
*res_layer1/branch2/conv2b_bn/batchnorm/mulMul,res_layer1/branch2/conv2b_bn/batchnorm/Rsqrt1res_layer1/branch2/conv2b_bn/conv2b_bn_scale/read*
T0
�
,res_layer1/branch2/conv2b_bn/batchnorm/mul_1Mul res_layer1/branch2/conv2b/conv2b*res_layer1/branch2/conv2b_bn/batchnorm/mul*
T0
�
,res_layer1/branch2/conv2b_bn/batchnorm/mul_2Mul6res_layer1/branch2/conv2b_bn/conv2b_bn_moments/Squeeze*res_layer1/branch2/conv2b_bn/batchnorm/mul*
T0
�
*res_layer1/branch2/conv2b_bn/batchnorm/subSub2res_layer1/branch2/conv2b_bn/conv2b_bn_offset/read,res_layer1/branch2/conv2b_bn/batchnorm/mul_2*
T0
�
,res_layer1/branch2/conv2b_bn/batchnorm/add_1Add,res_layer1/branch2/conv2b_bn/batchnorm/mul_1*res_layer1/branch2/conv2b_bn/batchnorm/sub*
T0
b
#res_layer1/branch2/conv2b_relu/reluRelu,res_layer1/branch2/conv2b_bn/batchnorm/add_1*
T0
�
Dres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/shapeConst*
dtype0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*%
valueB"            
�
Cres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
valueB
 *    *
dtype0
�
Eres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
valueB
 *E�=*
dtype0
�
Nres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/shape*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
dtype0*
seed2 *

seed 
�
Bres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/mulMulNres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalEres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights
�
>res_layer1/branch2/conv2c/weights/Initializer/truncated_normalAddBres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/mulCres_layer1/branch2/conv2c/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights
�
!res_layer1/branch2/conv2c/weights
VariableV2"/device:CPU:0*
shape:��*
shared_name *4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
dtype0*
	container 
�
(res_layer1/branch2/conv2c/weights/AssignAssign!res_layer1/branch2/conv2c/weights>res_layer1/branch2/conv2c/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
validate_shape(
�
&res_layer1/branch2/conv2c/weights/readIdentity!res_layer1/branch2/conv2c/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights
�
 res_layer1/branch2/conv2c/conv2cConv2D#res_layer1/branch2/conv2b_relu/relu&res_layer1/branch2/conv2c/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
~
Eres_layer1/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indicesConst*
dtype0*!
valueB"          
�
3res_layer1/branch2/conv2c_bn/conv2c_bn_moments/meanMean res_layer1/branch2/conv2c/conv2cEres_layer1/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
;res_layer1/branch2/conv2c_bn/conv2c_bn_moments/StopGradientStopGradient3res_layer1/branch2/conv2c_bn/conv2c_bn_moments/mean*
T0
�
@res_layer1/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceSquaredDifference res_layer1/branch2/conv2c/conv2c;res_layer1/branch2/conv2c_bn/conv2c_bn_moments/StopGradient*
T0
�
Ires_layer1/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indicesConst*
dtype0*!
valueB"          
�
7res_layer1/branch2/conv2c_bn/conv2c_bn_moments/varianceMean@res_layer1/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceIres_layer1/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer1/branch2/conv2c_bn/conv2c_bn_moments/SqueezeSqueeze3res_layer1/branch2/conv2c_bn/conv2c_bn_moments/mean*
squeeze_dims
 *
T0
�
8res_layer1/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1Squeeze7res_layer1/branch2/conv2c_bn/conv2c_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
valueB:�*
dtype0
�
Nres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulMulYres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale
�
Ires_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normalAddMres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulNres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale
�
,res_layer1/branch2/conv2c_bn/conv2c_bn_scale
VariableV2"/device:CPU:0*
shared_name *?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
dtype0*
	container *
shape:�
�
3res_layer1/branch2/conv2c_bn/conv2c_bn_scale/AssignAssign,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleIres_layer1/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
1res_layer1/branch2/conv2c_bn/conv2c_bn_scale/readIdentity,res_layer1/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale
�
Pres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
valueB:�*
dtype0
�
Ores_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shape*

seed *
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
dtype0*
seed2 
�
Nres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulMulZres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset
�
Jres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normalAddNres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulOres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset
�
-res_layer1/branch2/conv2c_bn/conv2c_bn_offset
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset
�
4res_layer1/branch2/conv2c_bn/conv2c_bn_offset/AssignAssign-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetJres_layer1/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset
�
2res_layer1/branch2/conv2c_bn/conv2c_bn_offset/readIdentity-res_layer1/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset
Y
,res_layer1/branch2/conv2c_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer1/branch2/conv2c_bn/batchnorm/addAdd8res_layer1/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1,res_layer1/branch2/conv2c_bn/batchnorm/add/y*
T0
j
,res_layer1/branch2/conv2c_bn/batchnorm/RsqrtRsqrt*res_layer1/branch2/conv2c_bn/batchnorm/add*
T0
�
*res_layer1/branch2/conv2c_bn/batchnorm/mulMul,res_layer1/branch2/conv2c_bn/batchnorm/Rsqrt1res_layer1/branch2/conv2c_bn/conv2c_bn_scale/read*
T0
�
,res_layer1/branch2/conv2c_bn/batchnorm/mul_1Mul res_layer1/branch2/conv2c/conv2c*res_layer1/branch2/conv2c_bn/batchnorm/mul*
T0
�
,res_layer1/branch2/conv2c_bn/batchnorm/mul_2Mul6res_layer1/branch2/conv2c_bn/conv2c_bn_moments/Squeeze*res_layer1/branch2/conv2c_bn/batchnorm/mul*
T0
�
*res_layer1/branch2/conv2c_bn/batchnorm/subSub2res_layer1/branch2/conv2c_bn/conv2c_bn_offset/read,res_layer1/branch2/conv2c_bn/batchnorm/mul_2*
T0
�
,res_layer1/branch2/conv2c_bn/batchnorm/add_1Add,res_layer1/branch2/conv2c_bn/batchnorm/mul_1*res_layer1/branch2/conv2c_bn/batchnorm/sub*
T0
~
res_layer1/plus/addAdd+res_layer1/branch1/conv1_bn/batchnorm/add_1,res_layer1/branch2/conv2c_bn/batchnorm/add_1*
T0
@
res_layer1/plus/final_reluRelures_layer1/plus/add*
T0
�
Cres_layer2/branch1/conv1/weights/Initializer/truncated_normal/shapeConst*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*%
valueB"            *
dtype0
�
Bres_layer2/branch1/conv1/weights/Initializer/truncated_normal/meanConst*
dtype0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
valueB
 *    
�
Dres_layer2/branch1/conv1/weights/Initializer/truncated_normal/stddevConst*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
valueB
 *E�=*
dtype0
�
Mres_layer2/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalCres_layer2/branch1/conv1/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
Ares_layer2/branch1/conv1/weights/Initializer/truncated_normal/mulMulMres_layer2/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalDres_layer2/branch1/conv1/weights/Initializer/truncated_normal/stddev*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
=res_layer2/branch1/conv1/weights/Initializer/truncated_normalAddAres_layer2/branch1/conv1/weights/Initializer/truncated_normal/mulBres_layer2/branch1/conv1/weights/Initializer/truncated_normal/mean*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
 res_layer2/branch1/conv1/weights
VariableV2"/device:CPU:0*
shared_name *3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
dtype0*
	container *
shape:��
�
'res_layer2/branch1/conv1/weights/AssignAssign res_layer2/branch1/conv1/weights=res_layer2/branch1/conv1/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
validate_shape(
�
%res_layer2/branch1/conv1/weights/readIdentity res_layer2/branch1/conv1/weights"/device:CPU:0*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
res_layer2/branch1/conv1/conv1Conv2Dres_layer1/plus/final_relu%res_layer2/branch1/conv1/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
�
Dres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*%
valueB"            *
dtype0
�
Cres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
valueB
 *    *
dtype0
�
Eres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
valueB
 *E�=*
dtype0
�
Nres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights
�
Bres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/mulMulNres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalEres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights
�
>res_layer2/branch2/conv2a/weights/Initializer/truncated_normalAddBres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/mulCres_layer2/branch2/conv2a/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights
�
!res_layer2/branch2/conv2a/weights
VariableV2"/device:CPU:0*
shared_name *4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
dtype0*
	container *
shape:��
�
(res_layer2/branch2/conv2a/weights/AssignAssign!res_layer2/branch2/conv2a/weights>res_layer2/branch2/conv2a/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
validate_shape(
�
&res_layer2/branch2/conv2a/weights/readIdentity!res_layer2/branch2/conv2a/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights
�
 res_layer2/branch2/conv2a/conv2aConv2Dres_layer1/plus/final_relu&res_layer2/branch2/conv2a/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
~
Eres_layer2/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer2/branch2/conv2a_bn/conv2a_bn_moments/meanMean res_layer2/branch2/conv2a/conv2aEres_layer2/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
;res_layer2/branch2/conv2a_bn/conv2a_bn_moments/StopGradientStopGradient3res_layer2/branch2/conv2a_bn/conv2a_bn_moments/mean*
T0
�
@res_layer2/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceSquaredDifference res_layer2/branch2/conv2a/conv2a;res_layer2/branch2/conv2a_bn/conv2a_bn_moments/StopGradient*
T0
�
Ires_layer2/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer2/branch2/conv2a_bn/conv2a_bn_moments/varianceMean@res_layer2/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceIres_layer2/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
6res_layer2/branch2/conv2a_bn/conv2a_bn_moments/SqueezeSqueeze3res_layer2/branch2/conv2a_bn/conv2a_bn_moments/mean*
squeeze_dims
 *
T0
�
8res_layer2/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1Squeeze7res_layer2/branch2/conv2a_bn/conv2a_bn_moments/variance*
T0*
squeeze_dims
 
�
Ores_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
valueB:�*
dtype0
�
Nres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulMulYres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale
�
Ires_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normalAddMres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulNres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale
�
,res_layer2/branch2/conv2a_bn/conv2a_bn_scale
VariableV2"/device:CPU:0*
shared_name *?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
dtype0*
	container *
shape:�
�
3res_layer2/branch2/conv2a_bn/conv2a_bn_scale/AssignAssign,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleIres_layer2/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(*
use_locking(
�
1res_layer2/branch2/conv2a_bn/conv2a_bn_scale/readIdentity,res_layer2/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale
�
Pres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
valueB:�*
dtype0
�
Ores_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddevConst*
dtype0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *�d�=
�
Zres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shape*

seed *
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
dtype0*
seed2 
�
Nres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulMulZres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset
�
Jres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normalAddNres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulOres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset
�
-res_layer2/branch2/conv2a_bn/conv2a_bn_offset
VariableV2"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
dtype0*
	container *
shape:�*
shared_name 
�
4res_layer2/branch2/conv2a_bn/conv2a_bn_offset/AssignAssign-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetJres_layer2/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset
�
2res_layer2/branch2/conv2a_bn/conv2a_bn_offset/readIdentity-res_layer2/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset
Y
,res_layer2/branch2/conv2a_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer2/branch2/conv2a_bn/batchnorm/addAdd8res_layer2/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1,res_layer2/branch2/conv2a_bn/batchnorm/add/y*
T0
j
,res_layer2/branch2/conv2a_bn/batchnorm/RsqrtRsqrt*res_layer2/branch2/conv2a_bn/batchnorm/add*
T0
�
*res_layer2/branch2/conv2a_bn/batchnorm/mulMul,res_layer2/branch2/conv2a_bn/batchnorm/Rsqrt1res_layer2/branch2/conv2a_bn/conv2a_bn_scale/read*
T0
�
,res_layer2/branch2/conv2a_bn/batchnorm/mul_1Mul res_layer2/branch2/conv2a/conv2a*res_layer2/branch2/conv2a_bn/batchnorm/mul*
T0
�
,res_layer2/branch2/conv2a_bn/batchnorm/mul_2Mul6res_layer2/branch2/conv2a_bn/conv2a_bn_moments/Squeeze*res_layer2/branch2/conv2a_bn/batchnorm/mul*
T0
�
*res_layer2/branch2/conv2a_bn/batchnorm/subSub2res_layer2/branch2/conv2a_bn/conv2a_bn_offset/read,res_layer2/branch2/conv2a_bn/batchnorm/mul_2*
T0
�
,res_layer2/branch2/conv2a_bn/batchnorm/add_1Add,res_layer2/branch2/conv2a_bn/batchnorm/mul_1*res_layer2/branch2/conv2a_bn/batchnorm/sub*
T0
b
#res_layer2/branch2/conv2a_relu/reluRelu,res_layer2/branch2/conv2a_bn/batchnorm/add_1*
T0
�
Dres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*%
valueB"            *
dtype0
�
Cres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/meanConst*
dtype0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
valueB
 *    
�
Eres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
valueB
 *�(=*
dtype0
�
Nres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
Bres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/mulMulNres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalEres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
>res_layer2/branch2/conv2b/weights/Initializer/truncated_normalAddBres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/mulCres_layer2/branch2/conv2b/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
!res_layer2/branch2/conv2b/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:��*
shared_name *4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
(res_layer2/branch2/conv2b/weights/AssignAssign!res_layer2/branch2/conv2b/weights>res_layer2/branch2/conv2b/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
validate_shape(
�
&res_layer2/branch2/conv2b/weights/readIdentity!res_layer2/branch2/conv2b/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
 res_layer2/branch2/conv2b/conv2bConv2D#res_layer2/branch2/conv2a_relu/relu&res_layer2/branch2/conv2b/weights/read*
paddingSAME*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
~
Eres_layer2/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer2/branch2/conv2b_bn/conv2b_bn_moments/meanMean res_layer2/branch2/conv2b/conv2bEres_layer2/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
;res_layer2/branch2/conv2b_bn/conv2b_bn_moments/StopGradientStopGradient3res_layer2/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0
�
@res_layer2/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceSquaredDifference res_layer2/branch2/conv2b/conv2b;res_layer2/branch2/conv2b_bn/conv2b_bn_moments/StopGradient*
T0
�
Ires_layer2/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer2/branch2/conv2b_bn/conv2b_bn_moments/varianceMean@res_layer2/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceIres_layer2/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer2/branch2/conv2b_bn/conv2b_bn_moments/SqueezeSqueeze3res_layer2/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0*
squeeze_dims
 
�
8res_layer2/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1Squeeze7res_layer2/branch2/conv2b_bn/conv2b_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shapeConst*
dtype0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
valueB:�
�
Nres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shape*

seed *
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
seed2 
�
Mres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulMulYres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale
�
Ires_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normalAddMres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulNres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale
�
,res_layer2/branch2/conv2b_bn/conv2b_bn_scale
VariableV2"/device:CPU:0*
shared_name *?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
	container *
shape:�
�
3res_layer2/branch2/conv2b_bn/conv2b_bn_scale/AssignAssign,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleIres_layer2/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
validate_shape(
�
1res_layer2/branch2/conv2b_bn/conv2b_bn_scale/readIdentity,res_layer2/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale
�
Pres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shapeConst*
dtype0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
valueB:�
�
Ores_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddevConst*
dtype0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *�d�=
�
Zres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset
�
Nres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulMulZres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset
�
Jres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normalAddNres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulOres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset
�
-res_layer2/branch2/conv2b_bn/conv2b_bn_offset
VariableV2"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
dtype0*
	container *
shape:�*
shared_name 
�
4res_layer2/branch2/conv2b_bn/conv2b_bn_offset/AssignAssign-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetJres_layer2/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
2res_layer2/branch2/conv2b_bn/conv2b_bn_offset/readIdentity-res_layer2/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset
Y
,res_layer2/branch2/conv2b_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer2/branch2/conv2b_bn/batchnorm/addAdd8res_layer2/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1,res_layer2/branch2/conv2b_bn/batchnorm/add/y*
T0
j
,res_layer2/branch2/conv2b_bn/batchnorm/RsqrtRsqrt*res_layer2/branch2/conv2b_bn/batchnorm/add*
T0
�
*res_layer2/branch2/conv2b_bn/batchnorm/mulMul,res_layer2/branch2/conv2b_bn/batchnorm/Rsqrt1res_layer2/branch2/conv2b_bn/conv2b_bn_scale/read*
T0
�
,res_layer2/branch2/conv2b_bn/batchnorm/mul_1Mul res_layer2/branch2/conv2b/conv2b*res_layer2/branch2/conv2b_bn/batchnorm/mul*
T0
�
,res_layer2/branch2/conv2b_bn/batchnorm/mul_2Mul6res_layer2/branch2/conv2b_bn/conv2b_bn_moments/Squeeze*res_layer2/branch2/conv2b_bn/batchnorm/mul*
T0
�
*res_layer2/branch2/conv2b_bn/batchnorm/subSub2res_layer2/branch2/conv2b_bn/conv2b_bn_offset/read,res_layer2/branch2/conv2b_bn/batchnorm/mul_2*
T0
�
,res_layer2/branch2/conv2b_bn/batchnorm/add_1Add,res_layer2/branch2/conv2b_bn/batchnorm/mul_1*res_layer2/branch2/conv2b_bn/batchnorm/sub*
T0
b
#res_layer2/branch2/conv2b_relu/reluRelu,res_layer2/branch2/conv2b_bn/batchnorm/add_1*
T0
�
Dres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*%
valueB"            *
dtype0
�
Cres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
valueB
 *    *
dtype0
�
Eres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/stddevConst*
dtype0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
valueB
 *E�=
�
Nres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights
�
Bres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/mulMulNres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalEres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights
�
>res_layer2/branch2/conv2c/weights/Initializer/truncated_normalAddBres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/mulCres_layer2/branch2/conv2c/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights
�
!res_layer2/branch2/conv2c/weights
VariableV2"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
dtype0*
	container *
shape:��*
shared_name 
�
(res_layer2/branch2/conv2c/weights/AssignAssign!res_layer2/branch2/conv2c/weights>res_layer2/branch2/conv2c/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
validate_shape(
�
&res_layer2/branch2/conv2c/weights/readIdentity!res_layer2/branch2/conv2c/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights
�
 res_layer2/branch2/conv2c/conv2cConv2D#res_layer2/branch2/conv2b_relu/relu&res_layer2/branch2/conv2c/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
~
Eres_layer2/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer2/branch2/conv2c_bn/conv2c_bn_moments/meanMean res_layer2/branch2/conv2c/conv2cEres_layer2/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
;res_layer2/branch2/conv2c_bn/conv2c_bn_moments/StopGradientStopGradient3res_layer2/branch2/conv2c_bn/conv2c_bn_moments/mean*
T0
�
@res_layer2/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceSquaredDifference res_layer2/branch2/conv2c/conv2c;res_layer2/branch2/conv2c_bn/conv2c_bn_moments/StopGradient*
T0
�
Ires_layer2/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer2/branch2/conv2c_bn/conv2c_bn_moments/varianceMean@res_layer2/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceIres_layer2/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer2/branch2/conv2c_bn/conv2c_bn_moments/SqueezeSqueeze3res_layer2/branch2/conv2c_bn/conv2c_bn_moments/mean*
squeeze_dims
 *
T0
�
8res_layer2/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1Squeeze7res_layer2/branch2/conv2c_bn/conv2c_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
valueB:�*
dtype0
�
Nres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/meanConst*
dtype0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *    
�
Pres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddevConst*
dtype0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *�d�=
�
Yres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulMulYres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale
�
Ires_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normalAddMres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulNres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale
�
,res_layer2/branch2/conv2c_bn/conv2c_bn_scale
VariableV2"/device:CPU:0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
dtype0*
	container *
shape:�*
shared_name 
�
3res_layer2/branch2/conv2c_bn/conv2c_bn_scale/AssignAssign,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleIres_layer2/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
1res_layer2/branch2/conv2c_bn/conv2c_bn_scale/readIdentity,res_layer2/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale
�
Pres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
valueB:�*
dtype0
�
Ores_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shape*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
dtype0*
seed2 *

seed 
�
Nres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulMulZres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset
�
Jres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normalAddNres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulOres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset
�
-res_layer2/branch2/conv2c_bn/conv2c_bn_offset
VariableV2"/device:CPU:0*
shape:�*
shared_name *@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
dtype0*
	container 
�
4res_layer2/branch2/conv2c_bn/conv2c_bn_offset/AssignAssign-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetJres_layer2/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(
�
2res_layer2/branch2/conv2c_bn/conv2c_bn_offset/readIdentity-res_layer2/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset
Y
,res_layer2/branch2/conv2c_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer2/branch2/conv2c_bn/batchnorm/addAdd8res_layer2/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1,res_layer2/branch2/conv2c_bn/batchnorm/add/y*
T0
j
,res_layer2/branch2/conv2c_bn/batchnorm/RsqrtRsqrt*res_layer2/branch2/conv2c_bn/batchnorm/add*
T0
�
*res_layer2/branch2/conv2c_bn/batchnorm/mulMul,res_layer2/branch2/conv2c_bn/batchnorm/Rsqrt1res_layer2/branch2/conv2c_bn/conv2c_bn_scale/read*
T0
�
,res_layer2/branch2/conv2c_bn/batchnorm/mul_1Mul res_layer2/branch2/conv2c/conv2c*res_layer2/branch2/conv2c_bn/batchnorm/mul*
T0
�
,res_layer2/branch2/conv2c_bn/batchnorm/mul_2Mul6res_layer2/branch2/conv2c_bn/conv2c_bn_moments/Squeeze*res_layer2/branch2/conv2c_bn/batchnorm/mul*
T0
�
*res_layer2/branch2/conv2c_bn/batchnorm/subSub2res_layer2/branch2/conv2c_bn/conv2c_bn_offset/read,res_layer2/branch2/conv2c_bn/batchnorm/mul_2*
T0
�
,res_layer2/branch2/conv2c_bn/batchnorm/add_1Add,res_layer2/branch2/conv2c_bn/batchnorm/mul_1*res_layer2/branch2/conv2c_bn/batchnorm/sub*
T0
q
res_layer2/plus/addAddres_layer2/branch1/conv1/conv1,res_layer2/branch2/conv2c_bn/batchnorm/add_1*
T0
@
res_layer2/plus/final_reluRelures_layer2/plus/add*
T0
�
Cres_layer3/branch1/conv1/weights/Initializer/truncated_normal/shapeConst*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*%
valueB"            *
dtype0
�
Bres_layer3/branch1/conv1/weights/Initializer/truncated_normal/meanConst*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
valueB
 *    *
dtype0
�
Dres_layer3/branch1/conv1/weights/Initializer/truncated_normal/stddevConst*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
valueB
 *E�=*
dtype0
�
Mres_layer3/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalCres_layer3/branch1/conv1/weights/Initializer/truncated_normal/shape*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
dtype0*
seed2 *

seed 
�
Ares_layer3/branch1/conv1/weights/Initializer/truncated_normal/mulMulMres_layer3/branch1/conv1/weights/Initializer/truncated_normal/TruncatedNormalDres_layer3/branch1/conv1/weights/Initializer/truncated_normal/stddev*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights
�
=res_layer3/branch1/conv1/weights/Initializer/truncated_normalAddAres_layer3/branch1/conv1/weights/Initializer/truncated_normal/mulBres_layer3/branch1/conv1/weights/Initializer/truncated_normal/mean*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights
�
 res_layer3/branch1/conv1/weights
VariableV2"/device:CPU:0*
shared_name *3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
dtype0*
	container *
shape:��
�
'res_layer3/branch1/conv1/weights/AssignAssign res_layer3/branch1/conv1/weights=res_layer3/branch1/conv1/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
validate_shape(
�
%res_layer3/branch1/conv1/weights/readIdentity res_layer3/branch1/conv1/weights"/device:CPU:0*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights
�
res_layer3/branch1/conv1/conv1Conv2Dres_layer2/plus/final_relu%res_layer3/branch1/conv1/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
�
Dres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/shapeConst*
dtype0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*%
valueB"            
�
Cres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
valueB
 *    *
dtype0
�
Eres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
valueB
 *E�=*
dtype0
�
Nres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/shape*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
dtype0*
seed2 *

seed 
�
Bres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/mulMulNres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/TruncatedNormalEres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights
�
>res_layer3/branch2/conv2a/weights/Initializer/truncated_normalAddBres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/mulCres_layer3/branch2/conv2a/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights
�
!res_layer3/branch2/conv2a/weights
VariableV2"/device:CPU:0*
shape:��*
shared_name *4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
dtype0*
	container 
�
(res_layer3/branch2/conv2a/weights/AssignAssign!res_layer3/branch2/conv2a/weights>res_layer3/branch2/conv2a/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
validate_shape(
�
&res_layer3/branch2/conv2a/weights/readIdentity!res_layer3/branch2/conv2a/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights
�
 res_layer3/branch2/conv2a/conv2aConv2Dres_layer2/plus/final_relu&res_layer3/branch2/conv2a/weights/read*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME*
	dilations
*
T0
~
Eres_layer3/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer3/branch2/conv2a_bn/conv2a_bn_moments/meanMean res_layer3/branch2/conv2a/conv2aEres_layer3/branch2/conv2a_bn/conv2a_bn_moments/mean/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
;res_layer3/branch2/conv2a_bn/conv2a_bn_moments/StopGradientStopGradient3res_layer3/branch2/conv2a_bn/conv2a_bn_moments/mean*
T0
�
@res_layer3/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceSquaredDifference res_layer3/branch2/conv2a/conv2a;res_layer3/branch2/conv2a_bn/conv2a_bn_moments/StopGradient*
T0
�
Ires_layer3/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer3/branch2/conv2a_bn/conv2a_bn_moments/varianceMean@res_layer3/branch2/conv2a_bn/conv2a_bn_moments/SquaredDifferenceIres_layer3/branch2/conv2a_bn/conv2a_bn_moments/variance/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
6res_layer3/branch2/conv2a_bn/conv2a_bn_moments/SqueezeSqueeze3res_layer3/branch2/conv2a_bn/conv2a_bn_moments/mean*
T0*
squeeze_dims
 
�
8res_layer3/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1Squeeze7res_layer3/branch2/conv2a_bn/conv2a_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
valueB:�*
dtype0
�
Nres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/meanConst*
dtype0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *    
�
Pres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/shape*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
dtype0*
seed2 *

seed 
�
Mres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulMulYres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale
�
Ires_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normalAddMres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mulNres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale
�
,res_layer3/branch2/conv2a_bn/conv2a_bn_scale
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale
�
3res_layer3/branch2/conv2a_bn/conv2a_bn_scale/AssignAssign,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleIres_layer3/branch2/conv2a_bn/conv2a_bn_scale/Initializer/truncated_normal"/device:CPU:0*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(*
use_locking(
�
1res_layer3/branch2/conv2a_bn/conv2a_bn_scale/readIdentity,res_layer3/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale
�
Pres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shapeConst*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
valueB:�
�
Ores_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/meanConst*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *    
�
Qres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/shape*

seed *
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
dtype0*
seed2 
�
Nres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulMulZres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset
�
Jres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normalAddNres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mulOres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset
�
-res_layer3/branch2/conv2a_bn/conv2a_bn_offset
VariableV2"/device:CPU:0*
shape:�*
shared_name *@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
dtype0*
	container 
�
4res_layer3/branch2/conv2a_bn/conv2a_bn_offset/AssignAssign-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetJres_layer3/branch2/conv2a_bn/conv2a_bn_offset/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(
�
2res_layer3/branch2/conv2a_bn/conv2a_bn_offset/readIdentity-res_layer3/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset
Y
,res_layer3/branch2/conv2a_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer3/branch2/conv2a_bn/batchnorm/addAdd8res_layer3/branch2/conv2a_bn/conv2a_bn_moments/Squeeze_1,res_layer3/branch2/conv2a_bn/batchnorm/add/y*
T0
j
,res_layer3/branch2/conv2a_bn/batchnorm/RsqrtRsqrt*res_layer3/branch2/conv2a_bn/batchnorm/add*
T0
�
*res_layer3/branch2/conv2a_bn/batchnorm/mulMul,res_layer3/branch2/conv2a_bn/batchnorm/Rsqrt1res_layer3/branch2/conv2a_bn/conv2a_bn_scale/read*
T0
�
,res_layer3/branch2/conv2a_bn/batchnorm/mul_1Mul res_layer3/branch2/conv2a/conv2a*res_layer3/branch2/conv2a_bn/batchnorm/mul*
T0
�
,res_layer3/branch2/conv2a_bn/batchnorm/mul_2Mul6res_layer3/branch2/conv2a_bn/conv2a_bn_moments/Squeeze*res_layer3/branch2/conv2a_bn/batchnorm/mul*
T0
�
*res_layer3/branch2/conv2a_bn/batchnorm/subSub2res_layer3/branch2/conv2a_bn/conv2a_bn_offset/read,res_layer3/branch2/conv2a_bn/batchnorm/mul_2*
T0
�
,res_layer3/branch2/conv2a_bn/batchnorm/add_1Add,res_layer3/branch2/conv2a_bn/batchnorm/mul_1*res_layer3/branch2/conv2a_bn/batchnorm/sub*
T0
b
#res_layer3/branch2/conv2a_relu/reluRelu,res_layer3/branch2/conv2a_bn/batchnorm/add_1*
T0
�
Dres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*%
valueB"            *
dtype0
�
Cres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
valueB
 *    *
dtype0
�
Eres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
valueB
 *�(=*
dtype0
�
Nres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/shape*

seed *
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
dtype0*
seed2 
�
Bres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/mulMulNres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/TruncatedNormalEres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights
�
>res_layer3/branch2/conv2b/weights/Initializer/truncated_normalAddBres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/mulCres_layer3/branch2/conv2b/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights
�
!res_layer3/branch2/conv2b/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:��*
shared_name *4
_class*
(&loc:@res_layer3/branch2/conv2b/weights
�
(res_layer3/branch2/conv2b/weights/AssignAssign!res_layer3/branch2/conv2b/weights>res_layer3/branch2/conv2b/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
validate_shape(
�
&res_layer3/branch2/conv2b/weights/readIdentity!res_layer3/branch2/conv2b/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights
�
 res_layer3/branch2/conv2b/conv2bConv2D#res_layer3/branch2/conv2a_relu/relu&res_layer3/branch2/conv2b/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
~
Eres_layer3/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indicesConst*!
valueB"          *
dtype0
�
3res_layer3/branch2/conv2b_bn/conv2b_bn_moments/meanMean res_layer3/branch2/conv2b/conv2bEres_layer3/branch2/conv2b_bn/conv2b_bn_moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
;res_layer3/branch2/conv2b_bn/conv2b_bn_moments/StopGradientStopGradient3res_layer3/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0
�
@res_layer3/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceSquaredDifference res_layer3/branch2/conv2b/conv2b;res_layer3/branch2/conv2b_bn/conv2b_bn_moments/StopGradient*
T0
�
Ires_layer3/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer3/branch2/conv2b_bn/conv2b_bn_moments/varianceMean@res_layer3/branch2/conv2b_bn/conv2b_bn_moments/SquaredDifferenceIres_layer3/branch2/conv2b_bn/conv2b_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer3/branch2/conv2b_bn/conv2b_bn_moments/SqueezeSqueeze3res_layer3/branch2/conv2b_bn/conv2b_bn_moments/mean*
T0*
squeeze_dims
 
�
8res_layer3/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1Squeeze7res_layer3/branch2/conv2b_bn/conv2b_bn_moments/variance*
T0*
squeeze_dims
 
�
Ores_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
valueB:�*
dtype0
�
Nres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddevConst*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
valueB
 *�d�=*
dtype0
�
Yres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/shape*

seed *
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
seed2 
�
Mres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulMulYres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale
�
Ires_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normalAddMres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mulNres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale
�
,res_layer3/branch2/conv2b_bn/conv2b_bn_scale
VariableV2"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
dtype0*
	container *
shape:�*
shared_name 
�
3res_layer3/branch2/conv2b_bn/conv2b_bn_scale/AssignAssign,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleIres_layer3/branch2/conv2b_bn/conv2b_bn_scale/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale
�
1res_layer3/branch2/conv2b_bn/conv2b_bn_scale/readIdentity,res_layer3/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale
�
Pres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
valueB:�*
dtype0
�
Ores_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/meanConst*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *    *
dtype0
�
Qres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/shape*

seed *
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
dtype0*
seed2 
�
Nres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulMulZres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset
�
Jres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normalAddNres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mulOres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset
�
-res_layer3/branch2/conv2b_bn/conv2b_bn_offset
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:�*
shared_name *@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset
�
4res_layer3/branch2/conv2b_bn/conv2b_bn_offset/AssignAssign-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetJres_layer3/branch2/conv2b_bn/conv2b_bn_offset/Initializer/truncated_normal"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(*
use_locking(
�
2res_layer3/branch2/conv2b_bn/conv2b_bn_offset/readIdentity-res_layer3/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset
Y
,res_layer3/branch2/conv2b_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer3/branch2/conv2b_bn/batchnorm/addAdd8res_layer3/branch2/conv2b_bn/conv2b_bn_moments/Squeeze_1,res_layer3/branch2/conv2b_bn/batchnorm/add/y*
T0
j
,res_layer3/branch2/conv2b_bn/batchnorm/RsqrtRsqrt*res_layer3/branch2/conv2b_bn/batchnorm/add*
T0
�
*res_layer3/branch2/conv2b_bn/batchnorm/mulMul,res_layer3/branch2/conv2b_bn/batchnorm/Rsqrt1res_layer3/branch2/conv2b_bn/conv2b_bn_scale/read*
T0
�
,res_layer3/branch2/conv2b_bn/batchnorm/mul_1Mul res_layer3/branch2/conv2b/conv2b*res_layer3/branch2/conv2b_bn/batchnorm/mul*
T0
�
,res_layer3/branch2/conv2b_bn/batchnorm/mul_2Mul6res_layer3/branch2/conv2b_bn/conv2b_bn_moments/Squeeze*res_layer3/branch2/conv2b_bn/batchnorm/mul*
T0
�
*res_layer3/branch2/conv2b_bn/batchnorm/subSub2res_layer3/branch2/conv2b_bn/conv2b_bn_offset/read,res_layer3/branch2/conv2b_bn/batchnorm/mul_2*
T0
�
,res_layer3/branch2/conv2b_bn/batchnorm/add_1Add,res_layer3/branch2/conv2b_bn/batchnorm/mul_1*res_layer3/branch2/conv2b_bn/batchnorm/sub*
T0
b
#res_layer3/branch2/conv2b_relu/reluRelu,res_layer3/branch2/conv2b_bn/batchnorm/add_1*
T0
�
Dres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/shapeConst*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*%
valueB"            *
dtype0
�
Cres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/meanConst*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*
valueB
 *    *
dtype0
�
Eres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/stddevConst*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*
valueB
 *E�=*
dtype0
�
Nres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormalDres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
Bres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/mulMulNres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/TruncatedNormalEres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/stddev*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
>res_layer3/branch2/conv2c/weights/Initializer/truncated_normalAddBres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/mulCres_layer3/branch2/conv2c/weights/Initializer/truncated_normal/mean*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
!res_layer3/branch2/conv2c/weights
VariableV2"/device:CPU:0*
shape:��*
shared_name *4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*
dtype0*
	container 
�
(res_layer3/branch2/conv2c/weights/AssignAssign!res_layer3/branch2/conv2c/weights>res_layer3/branch2/conv2c/weights/Initializer/truncated_normal"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
&res_layer3/branch2/conv2c/weights/readIdentity!res_layer3/branch2/conv2c/weights"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
 res_layer3/branch2/conv2c/conv2cConv2D#res_layer3/branch2/conv2b_relu/relu&res_layer3/branch2/conv2c/weights/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME
~
Eres_layer3/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indicesConst*
dtype0*!
valueB"          
�
3res_layer3/branch2/conv2c_bn/conv2c_bn_moments/meanMean res_layer3/branch2/conv2c/conv2cEres_layer3/branch2/conv2c_bn/conv2c_bn_moments/mean/reduction_indices*
T0*

Tidx0*
	keep_dims(
�
;res_layer3/branch2/conv2c_bn/conv2c_bn_moments/StopGradientStopGradient3res_layer3/branch2/conv2c_bn/conv2c_bn_moments/mean*
T0
�
@res_layer3/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceSquaredDifference res_layer3/branch2/conv2c/conv2c;res_layer3/branch2/conv2c_bn/conv2c_bn_moments/StopGradient*
T0
�
Ires_layer3/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indicesConst*!
valueB"          *
dtype0
�
7res_layer3/branch2/conv2c_bn/conv2c_bn_moments/varianceMean@res_layer3/branch2/conv2c_bn/conv2c_bn_moments/SquaredDifferenceIres_layer3/branch2/conv2c_bn/conv2c_bn_moments/variance/reduction_indices*

Tidx0*
	keep_dims(*
T0
�
6res_layer3/branch2/conv2c_bn/conv2c_bn_moments/SqueezeSqueeze3res_layer3/branch2/conv2c_bn/conv2c_bn_moments/mean*
squeeze_dims
 *
T0
�
8res_layer3/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1Squeeze7res_layer3/branch2/conv2c_bn/conv2c_bn_moments/variance*
squeeze_dims
 *
T0
�
Ores_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shapeConst*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
valueB:�*
dtype0
�
Nres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/meanConst*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *    *
dtype0
�
Pres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddevConst*
dtype0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
valueB
 *�d�=
�
Yres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalTruncatedNormalOres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale
�
Mres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulMulYres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/TruncatedNormalPres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/stddev*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale
�
Ires_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normalAddMres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mulNres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal/mean*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale
�
,res_layer3/branch2/conv2c_bn/conv2c_bn_scale
VariableV2"/device:CPU:0*
shared_name *?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
dtype0*
	container *
shape:�
�
3res_layer3/branch2/conv2c_bn/conv2c_bn_scale/AssignAssign,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleIres_layer3/branch2/conv2c_bn/conv2c_bn_scale/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
1res_layer3/branch2/conv2c_bn/conv2c_bn_scale/readIdentity,res_layer3/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale
�
Pres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shapeConst*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
valueB:�*
dtype0
�
Ores_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/meanConst*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *    
�
Qres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddevConst*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
valueB
 *�d�=*
dtype0
�
Zres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalTruncatedNormalPres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/shape*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
dtype0*
seed2 *

seed 
�
Nres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulMulZres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/TruncatedNormalQres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/stddev*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
�
Jres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normalAddNres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mulOres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal/mean*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
�
-res_layer3/branch2/conv2c_bn/conv2c_bn_offset
VariableV2"/device:CPU:0*
shape:�*
shared_name *@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
dtype0*
	container 
�
4res_layer3/branch2/conv2c_bn/conv2c_bn_offset/AssignAssign-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetJres_layer3/branch2/conv2c_bn/conv2c_bn_offset/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(
�
2res_layer3/branch2/conv2c_bn/conv2c_bn_offset/readIdentity-res_layer3/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
Y
,res_layer3/branch2/conv2c_bn/batchnorm/add/yConst*
valueB
 *��'7*
dtype0
�
*res_layer3/branch2/conv2c_bn/batchnorm/addAdd8res_layer3/branch2/conv2c_bn/conv2c_bn_moments/Squeeze_1,res_layer3/branch2/conv2c_bn/batchnorm/add/y*
T0
j
,res_layer3/branch2/conv2c_bn/batchnorm/RsqrtRsqrt*res_layer3/branch2/conv2c_bn/batchnorm/add*
T0
�
*res_layer3/branch2/conv2c_bn/batchnorm/mulMul,res_layer3/branch2/conv2c_bn/batchnorm/Rsqrt1res_layer3/branch2/conv2c_bn/conv2c_bn_scale/read*
T0
�
,res_layer3/branch2/conv2c_bn/batchnorm/mul_1Mul res_layer3/branch2/conv2c/conv2c*res_layer3/branch2/conv2c_bn/batchnorm/mul*
T0
�
,res_layer3/branch2/conv2c_bn/batchnorm/mul_2Mul6res_layer3/branch2/conv2c_bn/conv2c_bn_moments/Squeeze*res_layer3/branch2/conv2c_bn/batchnorm/mul*
T0
�
*res_layer3/branch2/conv2c_bn/batchnorm/subSub2res_layer3/branch2/conv2c_bn/conv2c_bn_offset/read,res_layer3/branch2/conv2c_bn/batchnorm/mul_2*
T0
�
,res_layer3/branch2/conv2c_bn/batchnorm/add_1Add,res_layer3/branch2/conv2c_bn/batchnorm/mul_1*res_layer3/branch2/conv2c_bn/batchnorm/sub*
T0
q
res_layer3/plus/addAddres_layer3/branch1/conv1/conv1,res_layer3/branch2/conv2c_bn/batchnorm/add_1*
T0
@
res_layer3/plus/final_reluRelures_layer3/plus/add*
T0
E
fea_rs/shapeConst*!
valueB"L  ,     *
dtype0
R
fea_rsReshaperes_layer3/plus/final_relufea_rs/shape*
T0*
Tshape0
H
BDGRU_rnn/BDGRU_rnn/fw/fw/RankConst*
value	B :*
dtype0
O
%BDGRU_rnn/BDGRU_rnn/fw/fw/range/startConst*
value	B :*
dtype0
O
%BDGRU_rnn/BDGRU_rnn/fw/fw/range/deltaConst*
value	B :*
dtype0
�
BDGRU_rnn/BDGRU_rnn/fw/fw/rangeRange%BDGRU_rnn/BDGRU_rnn/fw/fw/range/startBDGRU_rnn/BDGRU_rnn/fw/fw/Rank%BDGRU_rnn/BDGRU_rnn/fw/fw/range/delta*

Tidx0
^
)BDGRU_rnn/BDGRU_rnn/fw/fw/concat/values_0Const*
dtype0*
valueB"       
O
%BDGRU_rnn/BDGRU_rnn/fw/fw/concat/axisConst*
value	B : *
dtype0
�
 BDGRU_rnn/BDGRU_rnn/fw/fw/concatConcatV2)BDGRU_rnn/BDGRU_rnn/fw/fw/concat/values_0BDGRU_rnn/BDGRU_rnn/fw/fw/range%BDGRU_rnn/BDGRU_rnn/fw/fw/concat/axis*
N*

Tidx0*
T0
p
#BDGRU_rnn/BDGRU_rnn/fw/fw/transpose	Transposefea_rs BDGRU_rnn/BDGRU_rnn/fw/fw/concat*
T0*
Tperm0
M
)BDGRU_rnn/BDGRU_rnn/fw/fw/sequence_lengthIdentityPlaceholder_1*
T0
v
GBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/ConstConst*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_1Const*
dtype0*
valueB:d
w
MBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat/axisConst*
value	B : *
dtype0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concatConcatV2GBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/ConstIBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_1MBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat/axis*
T0*
N*

Tidx0
z
MBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros/ConstConst*
dtype0*
valueB
 *    
�
GBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zerosFillHBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concatMBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros/Const*
T0*

index_type0
x
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_2Const*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_3Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_4Const*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_5Const*
dtype0*
valueB:d
y
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1/axisConst*
dtype0*
value	B : 
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1ConcatV2IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_4IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_5OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1/axis*

Tidx0*
T0*
N
|
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1FillJBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1/Const*
T0*

index_type0
x
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_6Const*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/Const_7Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/ConstConst*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_1Const*
valueB:d*
dtype0
y
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat/axisConst*
value	B : *
dtype0
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concatConcatV2IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/ConstKBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_1OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat/axis*
T0*
N*

Tidx0
|
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zerosFillJBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concatOBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_2Const*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_3Const*
valueB:d*
dtype0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_4Const*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_5Const*
valueB:d*
dtype0
{
QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1/axisConst*
value	B : *
dtype0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1ConcatV2KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_4KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_5QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1/axis*

Tidx0*
T0*
N
~
QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1/ConstConst*
dtype0*
valueB
 *    
�
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1FillLBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_6Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_7Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/ConstConst*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_1Const*
dtype0*
valueB:d
y
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat/axisConst*
value	B : *
dtype0
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concatConcatV2IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/ConstKBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_1OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat/axis*
N*

Tidx0*
T0
|
OBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zerosFillJBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concatOBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_2Const*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_3Const*
valueB:d*
dtype0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_4Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_5Const*
valueB:d*
dtype0
{
QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1/axisConst*
dtype0*
value	B : 
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1ConcatV2KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_4KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_5QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1/axis*

Tidx0*
T0*
N
~
QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1/ConstConst*
valueB
 *    *
dtype0
�
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1FillLBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1QBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_6Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_7Const*
valueB:d*
dtype0
N
BDGRU_rnn/BDGRU_rnn/fw/fw/ShapeConst*
valueB:�*
dtype0
N
BDGRU_rnn/BDGRU_rnn/fw/fw/stackConst*
valueB:�*
dtype0
s
BDGRU_rnn/BDGRU_rnn/fw/fw/EqualEqualBDGRU_rnn/BDGRU_rnn/fw/fw/ShapeBDGRU_rnn/BDGRU_rnn/fw/fw/stack*
T0
M
BDGRU_rnn/BDGRU_rnn/fw/fw/ConstConst*
valueB: *
dtype0
�
BDGRU_rnn/BDGRU_rnn/fw/fw/AllAllBDGRU_rnn/BDGRU_rnn/fw/fw/EqualBDGRU_rnn/BDGRU_rnn/fw/fw/Const*

Tidx0*
	keep_dims( 
�
&BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/ConstConst*Z
valueQBO BIExpected shape for Tensor BDGRU_rnn/BDGRU_rnn/fw/fw/sequence_length:0 is *
dtype0
a
(BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Const_1Const*
dtype0*!
valueB B but saw shape: 
�
.BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Assert/data_0Const*
dtype0*Z
valueQBO BIExpected shape for Tensor BDGRU_rnn/BDGRU_rnn/fw/fw/sequence_length:0 is 
g
.BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Assert/data_2Const*!
valueB B but saw shape: *
dtype0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/AssertAssertBDGRU_rnn/BDGRU_rnn/fw/fw/All.BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Assert/data_0BDGRU_rnn/BDGRU_rnn/fw/fw/stack.BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Assert/data_2BDGRU_rnn/BDGRU_rnn/fw/fw/Shape*
T
2*
	summarize
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/CheckSeqLenIdentity)BDGRU_rnn/BDGRU_rnn/fw/fw/sequence_length(^BDGRU_rnn/BDGRU_rnn/fw/fw/Assert/Assert*
T0
Z
!BDGRU_rnn/BDGRU_rnn/fw/fw/Shape_1Const*!
valueB",  L     *
dtype0
[
-BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stackConst*
dtype0*
valueB: 
]
/BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stack_1Const*
dtype0*
valueB:
]
/BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stack_2Const*
valueB:*
dtype0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/strided_sliceStridedSlice!BDGRU_rnn/BDGRU_rnn/fw/fw/Shape_1-BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stack/BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stack_1/BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
T0*
Index0
P
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_1Const*
dtype0*
valueB:�
O
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_2Const*
dtype0*
valueB:d
Q
'BDGRU_rnn/BDGRU_rnn/fw/fw/concat_1/axisConst*
value	B : *
dtype0
�
"BDGRU_rnn/BDGRU_rnn/fw/fw/concat_1ConcatV2!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_1!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_2'BDGRU_rnn/BDGRU_rnn/fw/fw/concat_1/axis*
N*

Tidx0*
T0
R
%BDGRU_rnn/BDGRU_rnn/fw/fw/zeros/ConstConst*
valueB
 *    *
dtype0
�
BDGRU_rnn/BDGRU_rnn/fw/fw/zerosFill"BDGRU_rnn/BDGRU_rnn/fw/fw/concat_1%BDGRU_rnn/BDGRU_rnn/fw/fw/zeros/Const*
T0*

index_type0
O
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_3Const*
valueB: *
dtype0
�
BDGRU_rnn/BDGRU_rnn/fw/fw/MinMin%BDGRU_rnn/BDGRU_rnn/fw/fw/CheckSeqLen!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_3*

Tidx0*
	keep_dims( *
T0
O
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_4Const*
valueB: *
dtype0
�
BDGRU_rnn/BDGRU_rnn/fw/fw/MaxMax%BDGRU_rnn/BDGRU_rnn/fw/fw/CheckSeqLen!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_4*
T0*

Tidx0*
	keep_dims( 
H
BDGRU_rnn/BDGRU_rnn/fw/fw/timeConst*
dtype0*
value	B : 
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayTensorArrayV3'BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice*
dtype0*
element_shape:	�d*
clear_after_read(*
dynamic_size( *
identical_element_shapes(*E
tensor_array_name0.BDGRU_rnn/BDGRU_rnn/fw/fw/dynamic_rnn/output_0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray_1TensorArrayV3'BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice*
dtype0*
element_shape:
��*
dynamic_size( *
clear_after_read(*
identical_element_shapes(*D
tensor_array_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/dynamic_rnn/input_0
k
2BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/ShapeConst*!
valueB",  L     *
dtype0
n
@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stackConst*
valueB: *
dtype0
p
BBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stack_1Const*
valueB:*
dtype0
p
BBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stack_2Const*
dtype0*
valueB:
�
:BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_sliceStridedSlice2BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/Shape@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stackBBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stack_1BBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
T0*
Index0
b
8BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/range/startConst*
value	B : *
dtype0
b
8BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/range/deltaConst*
value	B :*
dtype0
�
2BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/rangeRange8BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/range/start:BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/strided_slice8BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/range/delta*

Tidx0
�
TBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/TensorArrayScatter/TensorArrayScatterV3TensorArrayScatterV3'BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray_12BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/range#BDGRU_rnn/BDGRU_rnn/fw/fw/transpose)BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray_1:1*
T0*6
_class,
*(loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/transpose
M
#BDGRU_rnn/BDGRU_rnn/fw/fw/Maximum/xConst*
value	B :*
dtype0
y
!BDGRU_rnn/BDGRU_rnn/fw/fw/MaximumMaximum#BDGRU_rnn/BDGRU_rnn/fw/fw/Maximum/xBDGRU_rnn/BDGRU_rnn/fw/fw/Max*
T0
�
!BDGRU_rnn/BDGRU_rnn/fw/fw/MinimumMinimum'BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice!BDGRU_rnn/BDGRU_rnn/fw/fw/Maximum*
T0
[
1BDGRU_rnn/BDGRU_rnn/fw/fw/while/iteration_counterConst*
value	B : *
dtype0
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/while/EnterEnter1BDGRU_rnn/BDGRU_rnn/fw/fw/while/iteration_counter*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_1EnterBDGRU_rnn/BDGRU_rnn/fw/fw/time*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context*
T0*
is_constant( 
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_2Enter'BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray:1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_3EnterGBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_4EnterIBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_5EnterIBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_6EnterKBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context*
T0*
is_constant( 
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_7EnterIBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context*
T0*
is_constant( 
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_8EnterKBDGRU_rnn/BDGRU_rnn/fw/fw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context*
T0*
is_constant( 
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/while/MergeMerge%BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter-BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_1Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_1/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_1*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_2Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_2/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_2*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_3Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_3/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_3*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_4Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_4/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_4*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_5Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_5/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_5*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_6Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_6/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_6*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_7Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_7/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_7*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_8Merge'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Enter_8/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_8*
T0*
N
�
$BDGRU_rnn/BDGRU_rnn/fw/fw/while/LessLess%BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less/Enter*
T0
�
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less/EnterEnter'BDGRU_rnn/BDGRU_rnn/fw/fw/strided_slice*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less_1Less'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_1,BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less_1/Enter*
T0
�
,BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less_1/EnterEnter!BDGRU_rnn/BDGRU_rnn/fw/fw/Minimum*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/LogicalAnd
LogicalAnd$BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Less_1
`
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCondLoopCond*BDGRU_rnn/BDGRU_rnn/fw/fw/while/LogicalAnd
�
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/SwitchSwitch%BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_1Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_1(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_1
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_2Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_2(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_2
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_3Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_3(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_3
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_4Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_4(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_4
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_5Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_5(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_5
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_6Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_6(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_6
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_7Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_7(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_7
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_8Switch'BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_8(BDGRU_rnn/BDGRU_rnn/fw/fw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/Merge_8
g
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/IdentityIdentity(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_1Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_1:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_2Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_2:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_3Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_3:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_4Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_4:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_5Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_5:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_6Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_6:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_7Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_7:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_8Identity*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_8:1*
T0
z
%BDGRU_rnn/BDGRU_rnn/fw/fw/while/add/yConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
#BDGRU_rnn/BDGRU_rnn/fw/fw/while/addAdd(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity%BDGRU_rnn/BDGRU_rnn/fw/fw/while/add/y*
T0
�
1BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3TensorArrayReadV37BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3/Enter*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_19BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3/Enter_1*
dtype0
�
7BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3/EnterEnter'BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray_1*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
9BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3/Enter_1EnterTBDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayUnstack/TensorArrayScatter/TensorArrayScatterV3*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqualGreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_12BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual/Enter*
T0
�
2BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual/EnterEnter%BDGRU_rnn/BDGRU_rnn/fw/fw/CheckSeqLen*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
TBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/shapeConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB"d  �  *
dtype0
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/minConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB
 *Js��*
dtype0
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB
 *Js�=*
dtype0
�
\BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/shape*
dtype0*
seed2 *

seed *
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
NBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
VariableV2*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0*
	container *
shape:
��*
shared_name 
�
:BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/AssignAssign3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelNBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
validate_shape(*
use_locking(
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/readIdentity3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
T0
�
CBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/Initializer/zerosConst*
dtype0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
valueB�*    
�
1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias
VariableV2*
dtype0*
	container *
shape:�*
shared_name *D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/AssignAssign1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasCBDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
validate_shape(
~
6BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/readIdentity1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/concatConcatV21BDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayReadV3*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_4NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/concat/axis*

Tidx0*
T0*
N
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/MatMul/Enter*
T0*
transpose_a( *
transpose_b( 
�
OBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd/Enter*
T0*
data_formatNHWC
�
PBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
dtype0*
value	B :
�
RBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
dtype0*
valueB
 *  �?
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_3*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/Tanh_1*
T0
�
TBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/shapeConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB"�   �  *
dtype0
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/minConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB
 *��̽*
dtype0
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB
 *���=*
dtype0
�
\BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/shape*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0*
seed2 *

seed 
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
NBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel
VariableV2*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0*
	container *
shape:
��*
shared_name 
�
:BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/AssignAssign3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelNBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform*
validate_shape(*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/readIdentity3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
T0
�
CBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/Initializer/zerosConst*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
valueB�*    *
dtype0
�
1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias
VariableV2*
dtype0*
	container *
shape:�*
shared_name *D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/AssignAssign1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasCBDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(
~
6BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/readIdentity1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
dtype0*
value	B :
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/concatConcatV2HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_2*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_6NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/concat/axis*
T0*
N*

Tidx0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/MatMul/Enter*
transpose_a( *
transpose_b( *
T0
�
OBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd/Enter*
T0*
data_formatNHWC
�
PBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
RBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
valueB
 *  �?*
dtype0
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_5*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/Tanh_1*
T0
�
TBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/shapeConst*
dtype0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB"�   �  
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/minConst*
dtype0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB
 *��̽
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB
 *���=*
dtype0
�
\BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/shape*

seed *
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0*
seed2 
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
RBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
NBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel
VariableV2*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0*
	container *
shape:
��*
shared_name 
�
:BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/AssignAssign3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelNBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(*
use_locking(
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/readIdentity3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
T0
�
CBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/Initializer/zerosConst*
dtype0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
valueB�*    
�
1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias
VariableV2*
dtype0*
	container *
shape:�*
shared_name *D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias
�
8BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/AssignAssign1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasCBDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
validate_shape(
~
6BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/readIdentity1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/concatConcatV2HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_2*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_8NBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/concat/axis*
N*

Tidx0*
T0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/MatMul/Enter*
transpose_b( *
T0*
transpose_a( 
�
OBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd/Enter*
data_formatNHWC*
T0
�
PBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
dtype0*
value	B :
�
RBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
valueB
 *  �?*
dtype0
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_7*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/Tanh_1*
T0
�
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/SelectSelect,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual,BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select/EnterHBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
,BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select/EnterEnterBDGRU_rnn/BDGRU_rnn/fw/fw/zeros*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_1Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_3HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_2Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_4HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_0/lstm_cell/mul_2
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_3Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_5HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_4Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_6HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_1/lstm_cell/mul_2
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_5Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_7HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_6Select,BDGRU_rnn/BDGRU_rnn/fw/fw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_8HBDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
CBDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayWrite/TensorArrayWriteV3TensorArrayWriteV3IBDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayWrite/TensorArrayWriteV3/Enter*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_1&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
IBDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayWrite/TensorArrayWriteV3/EnterEnter%BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/fw/fw/while/fw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/fw/fw/while/while_context
|
'BDGRU_rnn/BDGRU_rnn/fw/fw/while/add_1/yConst)^BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity*
value	B :*
dtype0
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/while/add_1Add*BDGRU_rnn/BDGRU_rnn/fw/fw/while/Identity_1'BDGRU_rnn/BDGRU_rnn/fw/fw/while/add_1/y*
T0
l
-BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIterationNextIteration#BDGRU_rnn/BDGRU_rnn/fw/fw/while/add*
T0
p
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_1NextIteration%BDGRU_rnn/BDGRU_rnn/fw/fw/while/add_1*
T0
�
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_2NextIterationCBDGRU_rnn/BDGRU_rnn/fw/fw/while/TensorArrayWrite/TensorArrayWriteV3*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_3NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_1*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_4NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_2*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_5NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_3*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_6NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_4*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_7NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_5*
T0
s
/BDGRU_rnn/BDGRU_rnn/fw/fw/while/NextIteration_8NextIteration(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Select_6*
T0
]
$BDGRU_rnn/BDGRU_rnn/fw/fw/while/ExitExit&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_1Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_1*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_2Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_2*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_3Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_3*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_4Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_4*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_5Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_5*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_6Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_6*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_7Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_7*
T0
a
&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_8Exit(BDGRU_rnn/BDGRU_rnn/fw/fw/while/Switch_8*
T0
�
<BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/TensorArraySizeV3TensorArraySizeV3%BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_2*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray
�
6BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/range/startConst*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray*
value	B : *
dtype0
�
6BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/range/deltaConst*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray*
value	B :*
dtype0
�
0BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/rangeRange6BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/range/start<BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/TensorArraySizeV36BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/range/delta*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray*

Tidx0
�
>BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/TensorArrayGatherV3TensorArrayGatherV3%BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray0BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/range&BDGRU_rnn/BDGRU_rnn/fw/fw/while/Exit_2*
element_shape:	�d*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArray*
dtype0
V
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_5Const*
valueB",  L  *
dtype0
O
!BDGRU_rnn/BDGRU_rnn/fw/fw/Const_6Const*
valueB:d*
dtype0
J
 BDGRU_rnn/BDGRU_rnn/fw/fw/Rank_1Const*
value	B :*
dtype0
Q
'BDGRU_rnn/BDGRU_rnn/fw/fw/range_1/startConst*
value	B :*
dtype0
Q
'BDGRU_rnn/BDGRU_rnn/fw/fw/range_1/deltaConst*
value	B :*
dtype0
�
!BDGRU_rnn/BDGRU_rnn/fw/fw/range_1Range'BDGRU_rnn/BDGRU_rnn/fw/fw/range_1/start BDGRU_rnn/BDGRU_rnn/fw/fw/Rank_1'BDGRU_rnn/BDGRU_rnn/fw/fw/range_1/delta*

Tidx0
`
+BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2/values_0Const*
valueB"       *
dtype0
Q
'BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2/axisConst*
value	B : *
dtype0
�
"BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2ConcatV2+BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2/values_0!BDGRU_rnn/BDGRU_rnn/fw/fw/range_1'BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2/axis*
T0*
N*

Tidx0
�
%BDGRU_rnn/BDGRU_rnn/fw/fw/transpose_1	Transpose>BDGRU_rnn/BDGRU_rnn/fw/fw/TensorArrayStack/TensorArrayGatherV3"BDGRU_rnn/BDGRU_rnn/fw/fw/concat_2*
T0*
Tperm0
�
&BDGRU_rnn/BDGRU_rnn/bw/ReverseSequenceReverseSequencefea_rsPlaceholder_1*
seq_dim*

Tlen0*
	batch_dim *
T0
H
BDGRU_rnn/BDGRU_rnn/bw/bw/RankConst*
dtype0*
value	B :
O
%BDGRU_rnn/BDGRU_rnn/bw/bw/range/startConst*
dtype0*
value	B :
O
%BDGRU_rnn/BDGRU_rnn/bw/bw/range/deltaConst*
dtype0*
value	B :
�
BDGRU_rnn/BDGRU_rnn/bw/bw/rangeRange%BDGRU_rnn/BDGRU_rnn/bw/bw/range/startBDGRU_rnn/BDGRU_rnn/bw/bw/Rank%BDGRU_rnn/BDGRU_rnn/bw/bw/range/delta*

Tidx0
^
)BDGRU_rnn/BDGRU_rnn/bw/bw/concat/values_0Const*
valueB"       *
dtype0
O
%BDGRU_rnn/BDGRU_rnn/bw/bw/concat/axisConst*
value	B : *
dtype0
�
 BDGRU_rnn/BDGRU_rnn/bw/bw/concatConcatV2)BDGRU_rnn/BDGRU_rnn/bw/bw/concat/values_0BDGRU_rnn/BDGRU_rnn/bw/bw/range%BDGRU_rnn/BDGRU_rnn/bw/bw/concat/axis*
T0*
N*

Tidx0
�
#BDGRU_rnn/BDGRU_rnn/bw/bw/transpose	Transpose&BDGRU_rnn/BDGRU_rnn/bw/ReverseSequence BDGRU_rnn/BDGRU_rnn/bw/bw/concat*
Tperm0*
T0
M
)BDGRU_rnn/BDGRU_rnn/bw/bw/sequence_lengthIdentityPlaceholder_1*
T0
v
GBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/ConstConst*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_1Const*
valueB:d*
dtype0
w
MBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat/axisConst*
dtype0*
value	B : 
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concatConcatV2GBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/ConstIBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_1MBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat/axis*
T0*
N*

Tidx0
z
MBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros/ConstConst*
valueB
 *    *
dtype0
�
GBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zerosFillHBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concatMBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros/Const*
T0*

index_type0
x
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_2Const*
dtype0*
valueB:�
w
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_3Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_4Const*
dtype0*
valueB:�
w
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_5Const*
valueB:d*
dtype0
y
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1/axisConst*
value	B : *
dtype0
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1ConcatV2IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_4IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_5OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1/axis*
T0*
N*

Tidx0
|
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1FillJBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/concat_1OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1/Const*
T0*

index_type0
x
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_6Const*
valueB:�*
dtype0
w
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/Const_7Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/ConstConst*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_1Const*
valueB:d*
dtype0
y
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat/axisConst*
value	B : *
dtype0
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concatConcatV2IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/ConstKBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_1OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat/axis*
T0*
N*

Tidx0
|
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zerosFillJBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concatOBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_2Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_3Const*
dtype0*
valueB:d
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_4Const*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_5Const*
valueB:d*
dtype0
{
QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1/axisConst*
dtype0*
value	B : 
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1ConcatV2KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_4KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_5QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1/axis*

Tidx0*
T0*
N
~
QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1/ConstConst*
valueB
 *    *
dtype0
�
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1FillLBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/concat_1QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_6Const*
dtype0*
valueB:�
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/Const_7Const*
valueB:d*
dtype0
x
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/ConstConst*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_1Const*
valueB:d*
dtype0
y
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat/axisConst*
dtype0*
value	B : 
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concatConcatV2IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/ConstKBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_1OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat/axis*
T0*
N*

Tidx0
|
OBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros/ConstConst*
valueB
 *    *
dtype0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zerosFillJBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concatOBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_2Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_3Const*
dtype0*
valueB:d
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_4Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_5Const*
dtype0*
valueB:d
{
QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1/axisConst*
value	B : *
dtype0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1ConcatV2KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_4KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_5QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1/axis*
N*

Tidx0*
T0
~
QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1/ConstConst*
valueB
 *    *
dtype0
�
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1FillLBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/concat_1QBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1/Const*
T0*

index_type0
z
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_6Const*
valueB:�*
dtype0
y
KBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/Const_7Const*
valueB:d*
dtype0
N
BDGRU_rnn/BDGRU_rnn/bw/bw/ShapeConst*
valueB:�*
dtype0
N
BDGRU_rnn/BDGRU_rnn/bw/bw/stackConst*
valueB:�*
dtype0
s
BDGRU_rnn/BDGRU_rnn/bw/bw/EqualEqualBDGRU_rnn/BDGRU_rnn/bw/bw/ShapeBDGRU_rnn/BDGRU_rnn/bw/bw/stack*
T0
M
BDGRU_rnn/BDGRU_rnn/bw/bw/ConstConst*
valueB: *
dtype0
�
BDGRU_rnn/BDGRU_rnn/bw/bw/AllAllBDGRU_rnn/BDGRU_rnn/bw/bw/EqualBDGRU_rnn/BDGRU_rnn/bw/bw/Const*

Tidx0*
	keep_dims( 
�
&BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/ConstConst*Z
valueQBO BIExpected shape for Tensor BDGRU_rnn/BDGRU_rnn/bw/bw/sequence_length:0 is *
dtype0
a
(BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Const_1Const*!
valueB B but saw shape: *
dtype0
�
.BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Assert/data_0Const*Z
valueQBO BIExpected shape for Tensor BDGRU_rnn/BDGRU_rnn/bw/bw/sequence_length:0 is *
dtype0
g
.BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Assert/data_2Const*
dtype0*!
valueB B but saw shape: 
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/AssertAssertBDGRU_rnn/BDGRU_rnn/bw/bw/All.BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Assert/data_0BDGRU_rnn/BDGRU_rnn/bw/bw/stack.BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Assert/data_2BDGRU_rnn/BDGRU_rnn/bw/bw/Shape*
T
2*
	summarize
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/CheckSeqLenIdentity)BDGRU_rnn/BDGRU_rnn/bw/bw/sequence_length(^BDGRU_rnn/BDGRU_rnn/bw/bw/Assert/Assert*
T0
Z
!BDGRU_rnn/BDGRU_rnn/bw/bw/Shape_1Const*
dtype0*!
valueB",  L     
[
-BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stackConst*
valueB: *
dtype0
]
/BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stack_1Const*
dtype0*
valueB:
]
/BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stack_2Const*
valueB:*
dtype0
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/strided_sliceStridedSlice!BDGRU_rnn/BDGRU_rnn/bw/bw/Shape_1-BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stack/BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stack_1/BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
P
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_1Const*
valueB:�*
dtype0
O
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_2Const*
valueB:d*
dtype0
Q
'BDGRU_rnn/BDGRU_rnn/bw/bw/concat_1/axisConst*
dtype0*
value	B : 
�
"BDGRU_rnn/BDGRU_rnn/bw/bw/concat_1ConcatV2!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_1!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_2'BDGRU_rnn/BDGRU_rnn/bw/bw/concat_1/axis*
N*

Tidx0*
T0
R
%BDGRU_rnn/BDGRU_rnn/bw/bw/zeros/ConstConst*
valueB
 *    *
dtype0
�
BDGRU_rnn/BDGRU_rnn/bw/bw/zerosFill"BDGRU_rnn/BDGRU_rnn/bw/bw/concat_1%BDGRU_rnn/BDGRU_rnn/bw/bw/zeros/Const*
T0*

index_type0
O
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_3Const*
valueB: *
dtype0
�
BDGRU_rnn/BDGRU_rnn/bw/bw/MinMin%BDGRU_rnn/BDGRU_rnn/bw/bw/CheckSeqLen!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_3*
T0*

Tidx0*
	keep_dims( 
O
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_4Const*
dtype0*
valueB: 
�
BDGRU_rnn/BDGRU_rnn/bw/bw/MaxMax%BDGRU_rnn/BDGRU_rnn/bw/bw/CheckSeqLen!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_4*
T0*

Tidx0*
	keep_dims( 
H
BDGRU_rnn/BDGRU_rnn/bw/bw/timeConst*
value	B : *
dtype0
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayTensorArrayV3'BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice*E
tensor_array_name0.BDGRU_rnn/BDGRU_rnn/bw/bw/dynamic_rnn/output_0*
dtype0*
element_shape:	�d*
clear_after_read(*
dynamic_size( *
identical_element_shapes(
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray_1TensorArrayV3'BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice*
dtype0*
element_shape:
��*
dynamic_size( *
clear_after_read(*
identical_element_shapes(*D
tensor_array_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/dynamic_rnn/input_0
k
2BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/ShapeConst*!
valueB",  L     *
dtype0
n
@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stackConst*
valueB: *
dtype0
p
BBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stack_1Const*
dtype0*
valueB:
p
BBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stack_2Const*
valueB:*
dtype0
�
:BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_sliceStridedSlice2BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/Shape@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stackBBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stack_1BBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
Index0*
T0
b
8BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/range/startConst*
dtype0*
value	B : 
b
8BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/range/deltaConst*
value	B :*
dtype0
�
2BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/rangeRange8BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/range/start:BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/strided_slice8BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/range/delta*

Tidx0
�
TBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/TensorArrayScatter/TensorArrayScatterV3TensorArrayScatterV3'BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray_12BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/range#BDGRU_rnn/BDGRU_rnn/bw/bw/transpose)BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray_1:1*
T0*6
_class,
*(loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/transpose
M
#BDGRU_rnn/BDGRU_rnn/bw/bw/Maximum/xConst*
value	B :*
dtype0
y
!BDGRU_rnn/BDGRU_rnn/bw/bw/MaximumMaximum#BDGRU_rnn/BDGRU_rnn/bw/bw/Maximum/xBDGRU_rnn/BDGRU_rnn/bw/bw/Max*
T0
�
!BDGRU_rnn/BDGRU_rnn/bw/bw/MinimumMinimum'BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice!BDGRU_rnn/BDGRU_rnn/bw/bw/Maximum*
T0
[
1BDGRU_rnn/BDGRU_rnn/bw/bw/while/iteration_counterConst*
value	B : *
dtype0
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/while/EnterEnter1BDGRU_rnn/BDGRU_rnn/bw/bw/while/iteration_counter*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context*
T0*
is_constant( 
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_1EnterBDGRU_rnn/BDGRU_rnn/bw/bw/time*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_2Enter'BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray:1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_3EnterGBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_4EnterIBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState/zeros_1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_5EnterIBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_6EnterKBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_1/zeros_1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_7EnterIBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_8EnterKBDGRU_rnn/BDGRU_rnn/bw/bw/MultiRNNCellZeroState/LSTMCellZeroState_2/zeros_1*
T0*
is_constant( *
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/while/MergeMerge%BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter-BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_1Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_1/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_1*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_2Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_2/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_2*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_3Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_3/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_3*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_4Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_4/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_4*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_5Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_5/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_5*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_6Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_6/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_6*
T0*
N
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_7Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_7/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_7*
N*
T0
�
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_8Merge'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Enter_8/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_8*
T0*
N
�
$BDGRU_rnn/BDGRU_rnn/bw/bw/while/LessLess%BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less/Enter*
T0
�
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less/EnterEnter'BDGRU_rnn/BDGRU_rnn/bw/bw/strided_slice*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context*
T0*
is_constant(
�
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less_1Less'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_1,BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less_1/Enter*
T0
�
,BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less_1/EnterEnter!BDGRU_rnn/BDGRU_rnn/bw/bw/Minimum*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/LogicalAnd
LogicalAnd$BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Less_1
`
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCondLoopCond*BDGRU_rnn/BDGRU_rnn/bw/bw/while/LogicalAnd
�
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/SwitchSwitch%BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_1Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_1(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_1
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_2Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_2(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_2
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_3Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_3(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_3
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_4Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_4(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_4
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_5Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_5(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_5
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_6Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_6(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_6
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_7Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_7(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_7
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_8Switch'BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_8(BDGRU_rnn/BDGRU_rnn/bw/bw/while/LoopCond*
T0*:
_class0
.,loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/Merge_8
g
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/IdentityIdentity(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_1Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_1:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_2Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_2:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_3Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_3:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_4Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_4:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_5Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_5:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_6Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_6:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_7Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_7:1*
T0
k
*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_8Identity*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_8:1*
T0
z
%BDGRU_rnn/BDGRU_rnn/bw/bw/while/add/yConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
#BDGRU_rnn/BDGRU_rnn/bw/bw/while/addAdd(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity%BDGRU_rnn/BDGRU_rnn/bw/bw/while/add/y*
T0
�
1BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3TensorArrayReadV37BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3/Enter*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_19BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3/Enter_1*
dtype0
�
7BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3/EnterEnter'BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray_1*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
9BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3/Enter_1EnterTBDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayUnstack/TensorArrayScatter/TensorArrayScatterV3*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqualGreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_12BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual/Enter*
T0
�
2BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual/EnterEnter%BDGRU_rnn/BDGRU_rnn/bw/bw/CheckSeqLen*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
TBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/shapeConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB"d  �  *
dtype0
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/minConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB
 *Js��*
dtype0
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
valueB
 *Js�=*
dtype0
�
\BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/shape*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0*
seed2 *

seed 
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
NBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel
VariableV2*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0*
	container *
shape:
��*
shared_name 
�
:BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/AssignAssign3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelNBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Initializer/random_uniform*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
validate_shape(*
use_locking(
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/readIdentity3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
T0
�
CBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/Initializer/zerosConst*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
valueB�*    *
dtype0
�
1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias
VariableV2*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
dtype0*
	container *
shape:�*
shared_name 
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/AssignAssign1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasCBDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/Initializer/zeros*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
validate_shape(*
use_locking(
~
6BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/readIdentity1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/concatConcatV21BDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayReadV3*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_4NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/concat/axis*
T0*
N*

Tidx0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/MatMul/Enter*
T0*
transpose_a( *
transpose_b( 
�
OBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd/Enter*
T0*
data_formatNHWC
�
PBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
dtype0*
value	B :
�
RBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
valueB
 *  �?*
dtype0
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_3*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/Tanh_1*
T0
�
TBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/shapeConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB"�   �  *
dtype0
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/minConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB
 *��̽*
dtype0
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
valueB
 *���=*
dtype0
�
\BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/shape*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0*
seed2 *

seed 
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
NBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel
VariableV2*
shape:
��*
shared_name *F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0*
	container 
�
:BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/AssignAssign3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelNBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Initializer/random_uniform*
validate_shape(*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/readIdentity3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
T0
�
CBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/Initializer/zerosConst*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
valueB�*    *
dtype0
�
1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias
VariableV2*
dtype0*
	container *
shape:�*
shared_name *D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/AssignAssign1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasCBDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(
~
6BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/readIdentity1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
dtype0*
value	B :
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/concatConcatV2HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_2*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_6NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/concat/axis*

Tidx0*
T0*
N
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/MatMul/Enter*
transpose_b( *
T0*
transpose_a( 
�
OBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd/Enter*
T0*
data_formatNHWC
�
PBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/read*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context*
T0*
is_constant(
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
RBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
dtype0*
valueB
 *  �?
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_5*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/Tanh_1*
T0
�
TBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/shapeConst*
dtype0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB"�   �  
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/minConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB
 *��̽*
dtype0
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/maxConst*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
valueB
 *���=*
dtype0
�
\BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRandomUniformTBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/shape*

seed *
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0*
seed2 
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/subSubRBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/maxRBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
RBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/mulMul\BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/RandomUniformRBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/sub*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
NBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniformAddRBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/mulRBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform/min*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel
�
3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel
VariableV2*
shape:
��*
shared_name *F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0*
	container 
�
:BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/AssignAssign3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelNBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Initializer/random_uniform*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/readIdentity3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
T0
�
CBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/Initializer/zerosConst*
dtype0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
valueB�*    
�
1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias
VariableV2*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
dtype0*
	container *
shape:�*
shared_name 
�
8BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/AssignAssign1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasCBDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
validate_shape(
~
6BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/readIdentity1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
T0
�
NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/concat/axisConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/concatConcatV2HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_2*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_8NBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/concat/axis*
N*

Tidx0*
T0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/MatMulMatMulIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/concatOBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/MatMul/Enter*
T0*
transpose_a( *
transpose_b( 
�
OBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/MatMul/EnterEnter8BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/BiasAddBiasAddIBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/MatMulPBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd/Enter*
T0*
data_formatNHWC
�
PBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd/EnterEnter6BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/read*
T0*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/ConstConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
RBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split/split_dimConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
dtype0*
value	B :
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/splitSplitRBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split/split_dimJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/BiasAdd*
T0*
	num_split
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add/yConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
valueB
 *  �?*
dtype0
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/addAddJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split:2HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add/y*
T0
�
JBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/SigmoidSigmoidFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add*
T0
�
FBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mulMulJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_7*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_1SigmoidHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split*
T0
�
GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/TanhTanhJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split:1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_1MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_1GBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Tanh*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add_1AddFBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mulHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_1*
T0
�
LBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_2SigmoidJBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/split:3*
T0
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Tanh_1TanhHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add_1*
T0
�
HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2MulLBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Sigmoid_2IBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/Tanh_1*
T0
�
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/SelectSelect,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual,BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select/EnterHBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
,BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select/EnterEnterBDGRU_rnn/BDGRU_rnn/bw/bw/zeros*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
is_constant(
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_1Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_3HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_2Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_4HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_0/lstm_cell/mul_2
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_3Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_5HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_4Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_6HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_1/lstm_cell/mul_2
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_5Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_7HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add_1*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/add_1
�
(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_6Select,BDGRU_rnn/BDGRU_rnn/bw/bw/while/GreaterEqual*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_8HBDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
CBDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayWrite/TensorArrayWriteV3TensorArrayWriteV3IBDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayWrite/TensorArrayWriteV3/Enter*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_1&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_2*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2
�
IBDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayWrite/TensorArrayWriteV3/EnterEnter%BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray*
T0*[
_classQ
OMloc:@BDGRU_rnn/BDGRU_rnn/bw/bw/while/bw/multi_rnn_cell/cell_2/lstm_cell/mul_2*
is_constant(*
parallel_iterations *=

frame_name/-BDGRU_rnn/BDGRU_rnn/bw/bw/while/while_context
|
'BDGRU_rnn/BDGRU_rnn/bw/bw/while/add_1/yConst)^BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity*
value	B :*
dtype0
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/while/add_1Add*BDGRU_rnn/BDGRU_rnn/bw/bw/while/Identity_1'BDGRU_rnn/BDGRU_rnn/bw/bw/while/add_1/y*
T0
l
-BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIterationNextIteration#BDGRU_rnn/BDGRU_rnn/bw/bw/while/add*
T0
p
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_1NextIteration%BDGRU_rnn/BDGRU_rnn/bw/bw/while/add_1*
T0
�
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_2NextIterationCBDGRU_rnn/BDGRU_rnn/bw/bw/while/TensorArrayWrite/TensorArrayWriteV3*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_3NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_1*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_4NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_2*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_5NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_3*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_6NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_4*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_7NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_5*
T0
s
/BDGRU_rnn/BDGRU_rnn/bw/bw/while/NextIteration_8NextIteration(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Select_6*
T0
]
$BDGRU_rnn/BDGRU_rnn/bw/bw/while/ExitExit&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_1Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_1*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_2Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_2*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_3Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_3*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_4Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_4*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_5Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_5*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_6Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_6*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_7Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_7*
T0
a
&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_8Exit(BDGRU_rnn/BDGRU_rnn/bw/bw/while/Switch_8*
T0
�
<BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/TensorArraySizeV3TensorArraySizeV3%BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_2*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray
�
6BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/range/startConst*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray*
value	B : *
dtype0
�
6BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/range/deltaConst*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray*
value	B :*
dtype0
�
0BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/rangeRange6BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/range/start<BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/TensorArraySizeV36BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/range/delta*

Tidx0*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray
�
>BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/TensorArrayGatherV3TensorArrayGatherV3%BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray0BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/range&BDGRU_rnn/BDGRU_rnn/bw/bw/while/Exit_2*
dtype0*
element_shape:	�d*8
_class.
,*loc:@BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArray
V
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_5Const*
valueB",  L  *
dtype0
O
!BDGRU_rnn/BDGRU_rnn/bw/bw/Const_6Const*
valueB:d*
dtype0
J
 BDGRU_rnn/BDGRU_rnn/bw/bw/Rank_1Const*
dtype0*
value	B :
Q
'BDGRU_rnn/BDGRU_rnn/bw/bw/range_1/startConst*
dtype0*
value	B :
Q
'BDGRU_rnn/BDGRU_rnn/bw/bw/range_1/deltaConst*
dtype0*
value	B :
�
!BDGRU_rnn/BDGRU_rnn/bw/bw/range_1Range'BDGRU_rnn/BDGRU_rnn/bw/bw/range_1/start BDGRU_rnn/BDGRU_rnn/bw/bw/Rank_1'BDGRU_rnn/BDGRU_rnn/bw/bw/range_1/delta*

Tidx0
`
+BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2/values_0Const*
valueB"       *
dtype0
Q
'BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2/axisConst*
value	B : *
dtype0
�
"BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2ConcatV2+BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2/values_0!BDGRU_rnn/BDGRU_rnn/bw/bw/range_1'BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2/axis*
T0*
N*

Tidx0
�
%BDGRU_rnn/BDGRU_rnn/bw/bw/transpose_1	Transpose>BDGRU_rnn/BDGRU_rnn/bw/bw/TensorArrayStack/TensorArrayGatherV3"BDGRU_rnn/BDGRU_rnn/bw/bw/concat_2*
Tperm0*
T0
�
BDGRU_rnn/ReverseSequenceReverseSequence%BDGRU_rnn/BDGRU_rnn/bw/bw/transpose_1Placeholder_1*
seq_dim*

Tlen0*
	batch_dim *
T0
L
"BDGRU_rnn/birnn_output_concat/axisConst*
value	B :*
dtype0
�
BDGRU_rnn/birnn_output_concatConcatV2%BDGRU_rnn/BDGRU_rnn/fw/fw/transpose_1BDGRU_rnn/ReverseSequence"BDGRU_rnn/birnn_output_concat/axis*
T0*
N*

Tidx0
�
8rnn_fnn_layer/weights/Initializer/truncated_normal/shapeConst*(
_class
loc:@rnn_fnn_layer/weights*
valueB"   d   *
dtype0
�
7rnn_fnn_layer/weights/Initializer/truncated_normal/meanConst*(
_class
loc:@rnn_fnn_layer/weights*
valueB
 *    *
dtype0
�
9rnn_fnn_layer/weights/Initializer/truncated_normal/stddevConst*(
_class
loc:@rnn_fnn_layer/weights*
valueB
 *���=*
dtype0
�
Brnn_fnn_layer/weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormal8rnn_fnn_layer/weights/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*(
_class
loc:@rnn_fnn_layer/weights
�
6rnn_fnn_layer/weights/Initializer/truncated_normal/mulMulBrnn_fnn_layer/weights/Initializer/truncated_normal/TruncatedNormal9rnn_fnn_layer/weights/Initializer/truncated_normal/stddev*
T0*(
_class
loc:@rnn_fnn_layer/weights
�
2rnn_fnn_layer/weights/Initializer/truncated_normalAdd6rnn_fnn_layer/weights/Initializer/truncated_normal/mul7rnn_fnn_layer/weights/Initializer/truncated_normal/mean*
T0*(
_class
loc:@rnn_fnn_layer/weights
�
rnn_fnn_layer/weights
VariableV2"/device:CPU:0*
dtype0*
	container *
shape
:d*
shared_name *(
_class
loc:@rnn_fnn_layer/weights
�
rnn_fnn_layer/weights/AssignAssignrnn_fnn_layer/weights2rnn_fnn_layer/weights/Initializer/truncated_normal"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@rnn_fnn_layer/weights*
validate_shape(

rnn_fnn_layer/weights/readIdentityrnn_fnn_layer/weights"/device:CPU:0*
T0*(
_class
loc:@rnn_fnn_layer/weights
|
$rnn_fnn_layer/bias/Initializer/zerosConst*%
_class
loc:@rnn_fnn_layer/bias*
valueBd*    *
dtype0
�
rnn_fnn_layer/bias
VariableV2"/device:CPU:0*
dtype0*
	container *
shape:d*
shared_name *%
_class
loc:@rnn_fnn_layer/bias
�
rnn_fnn_layer/bias/AssignAssignrnn_fnn_layer/bias$rnn_fnn_layer/bias/Initializer/zeros"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@rnn_fnn_layer/bias*
validate_shape(
v
rnn_fnn_layer/bias/readIdentityrnn_fnn_layer/bias"/device:CPU:0*
T0*%
_class
loc:@rnn_fnn_layer/bias
�
>rnn_fnn_layer/weights_class/Initializer/truncated_normal/shapeConst*.
_class$
" loc:@rnn_fnn_layer/weights_class*
valueB"d      *
dtype0
�
=rnn_fnn_layer/weights_class/Initializer/truncated_normal/meanConst*.
_class$
" loc:@rnn_fnn_layer/weights_class*
valueB
 *    *
dtype0
�
?rnn_fnn_layer/weights_class/Initializer/truncated_normal/stddevConst*
dtype0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
valueB
 *��>
�
Hrnn_fnn_layer/weights_class/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>rnn_fnn_layer/weights_class/Initializer/truncated_normal/shape*
dtype0*
seed2 *

seed *
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class
�
<rnn_fnn_layer/weights_class/Initializer/truncated_normal/mulMulHrnn_fnn_layer/weights_class/Initializer/truncated_normal/TruncatedNormal?rnn_fnn_layer/weights_class/Initializer/truncated_normal/stddev*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class
�
8rnn_fnn_layer/weights_class/Initializer/truncated_normalAdd<rnn_fnn_layer/weights_class/Initializer/truncated_normal/mul=rnn_fnn_layer/weights_class/Initializer/truncated_normal/mean*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class
�
rnn_fnn_layer/weights_class
VariableV2"/device:CPU:0*
shape
:d*
shared_name *.
_class$
" loc:@rnn_fnn_layer/weights_class*
dtype0*
	container 
�
"rnn_fnn_layer/weights_class/AssignAssignrnn_fnn_layer/weights_class8rnn_fnn_layer/weights_class/Initializer/truncated_normal"/device:CPU:0*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
validate_shape(*
use_locking(
�
 rnn_fnn_layer/weights_class/readIdentityrnn_fnn_layer/weights_class"/device:CPU:0*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class
�
*rnn_fnn_layer/bias_class/Initializer/zerosConst*+
_class!
loc:@rnn_fnn_layer/bias_class*
valueB*    *
dtype0
�
rnn_fnn_layer/bias_class
VariableV2"/device:CPU:0*
shape:*
shared_name *+
_class!
loc:@rnn_fnn_layer/bias_class*
dtype0*
	container 
�
rnn_fnn_layer/bias_class/AssignAssignrnn_fnn_layer/bias_class*rnn_fnn_layer/bias_class/Initializer/zeros"/device:CPU:0*
T0*+
_class!
loc:@rnn_fnn_layer/bias_class*
validate_shape(*
use_locking(
�
rnn_fnn_layer/bias_class/readIdentityrnn_fnn_layer/bias_class"/device:CPU:0*
T0*+
_class!
loc:@rnn_fnn_layer/bias_class
Y
rnn_fnn_layer/lasth_rs/shapeConst*%
valueB"L  ,     d   *
dtype0
u
rnn_fnn_layer/lasth_rsReshapeBDGRU_rnn/birnn_output_concatrnn_fnn_layer/lasth_rs/shape*
T0*
Tshape0
U
rnn_fnn_layer/MulMulrnn_fnn_layer/lasth_rsrnn_fnn_layer/weights/read*
T0
M
#rnn_fnn_layer/Sum/reduction_indicesConst*
dtype0*
value	B :
v
rnn_fnn_layer/SumSumrnn_fnn_layer/Mul#rnn_fnn_layer/Sum/reduction_indices*
T0*

Tidx0*
	keep_dims( 
s
rnn_fnn_layer/lasth_bias_addBiasAddrnn_fnn_layer/Sumrnn_fnn_layer/bias/read*
T0*
data_formatNHWC
Q
rnn_fnn_layer/lasto_rs/shapeConst*
dtype0*
valueB"	 d   
t
rnn_fnn_layer/lasto_rsReshapernn_fnn_layer/lasth_bias_addrnn_fnn_layer/lasto_rs/shape*
T0*
Tshape0
�
rnn_fnn_layer/MatMulMatMulrnn_fnn_layer/lasto_rs rnn_fnn_layer/weights_class/read*
T0*
transpose_a( *
transpose_b( 
u
rnn_fnn_layer/BiasAddBiasAddrnn_fnn_layer/MatMulrnn_fnn_layer/bias_class/read*
data_formatNHWC*
T0
Z
!rnn_fnn_layer/rnn_logits_rs/shapeConst*!
valueB"L  ,     *
dtype0
w
rnn_fnn_layer/rnn_logits_rsReshapernn_fnn_layer/BiasAdd!rnn_fnn_layer/rnn_logits_rs/shape*
T0*
Tshape0
6
Placeholder_3Placeholder*
shape: *
dtype0
6
Placeholder_4Placeholder*
shape: *
dtype0
�

fifo_queueFIFOQueueV2*
shared_name *
capacity�*
component_types
2*
	container *'
shapes
:��: : :�
*
fifo_queue_SizeQueueSizeV2
fifo_queue
�
fifo_queue_enqueueQueueEnqueueV2
fifo_queuernn_fnn_layer/rnn_logits_rsPlaceholder_4Placeholder_3Placeholder_1*
Tcomponents
2*

timeout_ms���������
K
fifo_queue_CloseQueueCloseV2
fifo_queue*
cancel_pending_enqueues( 
h
fifo_queue_DequeueQueueDequeueV2
fifo_queue*
component_types
2*

timeout_ms���������
2
ConstConst*
dtype0*
valueB
 *    
G
transpose/permConst*!
valueB"          *
dtype0
P
	transpose	Transposefifo_queue_Dequeuetranspose/perm*
Tperm0*
T0
�
CTCBeamSearchDecoderCTCBeamSearchDecoder	transposefifo_queue_Dequeue:3*
	top_paths*

beam_width2*
merge_repeated( 
}
fifo_queue_1FIFOQueueV2*
shapes
 *
shared_name *
capacity*
component_types
	2			*
	container 
�
fifo_queue_1_enqueueQueueEnqueueV2fifo_queue_1CTCBeamSearchDecoderCTCBeamSearchDecoder:1CTCBeamSearchDecoder:2CTCBeamSearchDecoder:3Constfifo_queue_Dequeue:1fifo_queue_Dequeue:2*
Tcomponents
	2			*

timeout_ms���������
o
fifo_queue_1_DequeueQueueDequeueV2fifo_queue_1*
component_types
	2			*

timeout_ms���������
O
fifo_queue_1_CloseQueueCloseV2fifo_queue_1*
cancel_pending_enqueues( 
Q
fifo_queue_1_Close_1QueueCloseV2fifo_queue_1*
cancel_pending_enqueues(
.
fifo_queue_1_SizeQueueSizeV2fifo_queue_1
A
save/filename/inputConst*
valueB Bmodel*
dtype0
V
save/filenamePlaceholderWithDefaultsave/filename/input*
dtype0*
shape: 
M

save/ConstPlaceholderWithDefaultsave/filename*
dtype0*
shape: 
�
save/SaveV2/tensor_namesConst*�
value�B�0B1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelB res_layer1/branch1/conv1/weightsB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB!res_layer1/branch2/conv2a/weightsB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer1/branch2/conv2b/weightsB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer1/branch2/conv2c/weightsB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer2/branch2/conv2b/weightsB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer2/branch2/conv2c/weightsB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer3/branch2/conv2b/weightsB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer3/branch2/conv2c/weightsB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleBrnn_fnn_layer/biasBrnn_fnn_layer/bias_classBrnn_fnn_layer/weightsBrnn_fnn_layer/weights_class*
dtype0
�
save/SaveV2/shape_and_slicesConst*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0
�
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slices1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel res_layer1/branch1/conv1/weights+res_layer1/branch1/conv1_bn/conv1_bn_offset*res_layer1/branch1/conv1_bn/conv1_bn_scale!res_layer1/branch2/conv2a/weights-res_layer1/branch2/conv2a_bn/conv2a_bn_offset,res_layer1/branch2/conv2a_bn/conv2a_bn_scale!res_layer1/branch2/conv2b/weights-res_layer1/branch2/conv2b_bn/conv2b_bn_offset,res_layer1/branch2/conv2b_bn/conv2b_bn_scale!res_layer1/branch2/conv2c/weights-res_layer1/branch2/conv2c_bn/conv2c_bn_offset,res_layer1/branch2/conv2c_bn/conv2c_bn_scale res_layer2/branch1/conv1/weights!res_layer2/branch2/conv2a/weights-res_layer2/branch2/conv2a_bn/conv2a_bn_offset,res_layer2/branch2/conv2a_bn/conv2a_bn_scale!res_layer2/branch2/conv2b/weights-res_layer2/branch2/conv2b_bn/conv2b_bn_offset,res_layer2/branch2/conv2b_bn/conv2b_bn_scale!res_layer2/branch2/conv2c/weights-res_layer2/branch2/conv2c_bn/conv2c_bn_offset,res_layer2/branch2/conv2c_bn/conv2c_bn_scale res_layer3/branch1/conv1/weights!res_layer3/branch2/conv2a/weights-res_layer3/branch2/conv2a_bn/conv2a_bn_offset,res_layer3/branch2/conv2a_bn/conv2a_bn_scale!res_layer3/branch2/conv2b/weights-res_layer3/branch2/conv2b_bn/conv2b_bn_offset,res_layer3/branch2/conv2b_bn/conv2b_bn_scale!res_layer3/branch2/conv2c/weights-res_layer3/branch2/conv2c_bn/conv2c_bn_offset,res_layer3/branch2/conv2c_bn/conv2c_bn_scalernn_fnn_layer/biasrnn_fnn_layer/bias_classrnn_fnn_layer/weightsrnn_fnn_layer/weights_class*>
dtypes4
220
e
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const
�
save/RestoreV2/tensor_namesConst"/device:CPU:0*�
value�B�0B1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelB res_layer1/branch1/conv1/weightsB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB!res_layer1/branch2/conv2a/weightsB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer1/branch2/conv2b/weightsB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer1/branch2/conv2c/weightsB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer2/branch2/conv2b/weightsB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer2/branch2/conv2c/weightsB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer3/branch2/conv2b/weightsB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer3/branch2/conv2c/weightsB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleBrnn_fnn_layer/biasBrnn_fnn_layer/bias_classBrnn_fnn_layer/weightsBrnn_fnn_layer/weights_class*
dtype0
�
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*s
valuejBh0B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0
�
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*>
dtypes4
220
�
save/AssignAssign1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biassave/RestoreV2*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
validate_shape(
�
save/Assign_1Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelsave/RestoreV2:1*
validate_shape(*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
save/Assign_2Assign1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biassave/RestoreV2:2*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(*
use_locking(
�
save/Assign_3Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelsave/RestoreV2:3*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
validate_shape(
�
save/Assign_4Assign1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biassave/RestoreV2:4*
validate_shape(*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias
�
save/Assign_5Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelsave/RestoreV2:5*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(*
use_locking(
�
save/Assign_6Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biassave/RestoreV2:6*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
validate_shape(*
use_locking(
�
save/Assign_7Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelsave/RestoreV2:7*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
validate_shape(
�
save/Assign_8Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biassave/RestoreV2:8*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(*
use_locking(
�
save/Assign_9Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelsave/RestoreV2:9*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
validate_shape(
�
save/Assign_10Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biassave/RestoreV2:10*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
validate_shape(*
use_locking(
�
save/Assign_11Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelsave/RestoreV2:11*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(
�
save/Assign_12Assign res_layer1/branch1/conv1/weightssave/RestoreV2:12"/device:CPU:0*
validate_shape(*
use_locking(*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
save/Assign_13Assign+res_layer1/branch1/conv1_bn/conv1_bn_offsetsave/RestoreV2:13"/device:CPU:0*
use_locking(*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
validate_shape(
�
save/Assign_14Assign*res_layer1/branch1/conv1_bn/conv1_bn_scalesave/RestoreV2:14"/device:CPU:0*
use_locking(*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
validate_shape(
�
save/Assign_15Assign!res_layer1/branch2/conv2a/weightssave/RestoreV2:15"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
validate_shape(
�
save/Assign_16Assign-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetsave/RestoreV2:16"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
�
save/Assign_17Assign,res_layer1/branch2/conv2a_bn/conv2a_bn_scalesave/RestoreV2:17"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale
�
save/Assign_18Assign!res_layer1/branch2/conv2b/weightssave/RestoreV2:18"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
validate_shape(
�
save/Assign_19Assign-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetsave/RestoreV2:19"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
save/Assign_20Assign,res_layer1/branch2/conv2b_bn/conv2b_bn_scalesave/RestoreV2:20"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
save/Assign_21Assign!res_layer1/branch2/conv2c/weightssave/RestoreV2:21"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
validate_shape(
�
save/Assign_22Assign-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetsave/RestoreV2:22"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(
�
save/Assign_23Assign,res_layer1/branch2/conv2c_bn/conv2c_bn_scalesave/RestoreV2:23"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
save/Assign_24Assign res_layer2/branch1/conv1/weightssave/RestoreV2:24"/device:CPU:0*
validate_shape(*
use_locking(*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
save/Assign_25Assign!res_layer2/branch2/conv2a/weightssave/RestoreV2:25"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
validate_shape(*
use_locking(
�
save/Assign_26Assign-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetsave/RestoreV2:26"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset
�
save/Assign_27Assign,res_layer2/branch2/conv2a_bn/conv2a_bn_scalesave/RestoreV2:27"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(*
use_locking(
�
save/Assign_28Assign!res_layer2/branch2/conv2b/weightssave/RestoreV2:28"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
validate_shape(
�
save/Assign_29Assign-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetsave/RestoreV2:29"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(*
use_locking(
�
save/Assign_30Assign,res_layer2/branch2/conv2b_bn/conv2b_bn_scalesave/RestoreV2:30"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
validate_shape(*
use_locking(
�
save/Assign_31Assign!res_layer2/branch2/conv2c/weightssave/RestoreV2:31"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
validate_shape(
�
save/Assign_32Assign-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetsave/RestoreV2:32"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset
�
save/Assign_33Assign,res_layer2/branch2/conv2c_bn/conv2c_bn_scalesave/RestoreV2:33"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale
�
save/Assign_34Assign res_layer3/branch1/conv1/weightssave/RestoreV2:34"/device:CPU:0*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
validate_shape(*
use_locking(
�
save/Assign_35Assign!res_layer3/branch2/conv2a/weightssave/RestoreV2:35"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights
�
save/Assign_36Assign-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetsave/RestoreV2:36"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(
�
save/Assign_37Assign,res_layer3/branch2/conv2a_bn/conv2a_bn_scalesave/RestoreV2:37"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(
�
save/Assign_38Assign!res_layer3/branch2/conv2b/weightssave/RestoreV2:38"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
validate_shape(*
use_locking(
�
save/Assign_39Assign-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetsave/RestoreV2:39"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
save/Assign_40Assign,res_layer3/branch2/conv2b_bn/conv2b_bn_scalesave/RestoreV2:40"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
validate_shape(
�
save/Assign_41Assign!res_layer3/branch2/conv2c/weightssave/RestoreV2:41"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
save/Assign_42Assign-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetsave/RestoreV2:42"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(*
use_locking(
�
save/Assign_43Assign,res_layer3/branch2/conv2c_bn/conv2c_bn_scalesave/RestoreV2:43"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
save/Assign_44Assignrnn_fnn_layer/biassave/RestoreV2:44"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@rnn_fnn_layer/bias*
validate_shape(
�
save/Assign_45Assignrnn_fnn_layer/bias_classsave/RestoreV2:45"/device:CPU:0*
use_locking(*
T0*+
_class!
loc:@rnn_fnn_layer/bias_class*
validate_shape(
�
save/Assign_46Assignrnn_fnn_layer/weightssave/RestoreV2:46"/device:CPU:0*
T0*(
_class
loc:@rnn_fnn_layer/weights*
validate_shape(*
use_locking(
�
save/Assign_47Assignrnn_fnn_layer/weights_classsave/RestoreV2:47"/device:CPU:0*
use_locking(*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
validate_shape(
�
save/restore_all/NoOpNoOp^save/Assign^save/Assign_1^save/Assign_10^save/Assign_11^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9
�
save/restore_all/NoOp_1NoOp^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_30^save/Assign_31^save/Assign_32^save/Assign_33^save/Assign_34^save/Assign_35^save/Assign_36^save/Assign_37^save/Assign_38^save/Assign_39^save/Assign_40^save/Assign_41^save/Assign_42^save/Assign_43^save/Assign_44^save/Assign_45^save/Assign_46^save/Assign_47"/device:CPU:0
J
save/restore_allNoOp^save/restore_all/NoOp^save/restore_all/NoOp_1
�
	init/NoOpNoOp9^BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias/Assign;^BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel/Assign9^BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias/Assign;^BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel/Assign9^BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias/Assign;^BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel/Assign9^BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias/Assign;^BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel/Assign9^BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias/Assign;^BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel/Assign9^BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias/Assign;^BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel/Assign
�
init/NoOp_1NoOp(^res_layer1/branch1/conv1/weights/Assign3^res_layer1/branch1/conv1_bn/conv1_bn_offset/Assign2^res_layer1/branch1/conv1_bn/conv1_bn_scale/Assign)^res_layer1/branch2/conv2a/weights/Assign5^res_layer1/branch2/conv2a_bn/conv2a_bn_offset/Assign4^res_layer1/branch2/conv2a_bn/conv2a_bn_scale/Assign)^res_layer1/branch2/conv2b/weights/Assign5^res_layer1/branch2/conv2b_bn/conv2b_bn_offset/Assign4^res_layer1/branch2/conv2b_bn/conv2b_bn_scale/Assign)^res_layer1/branch2/conv2c/weights/Assign5^res_layer1/branch2/conv2c_bn/conv2c_bn_offset/Assign4^res_layer1/branch2/conv2c_bn/conv2c_bn_scale/Assign(^res_layer2/branch1/conv1/weights/Assign)^res_layer2/branch2/conv2a/weights/Assign5^res_layer2/branch2/conv2a_bn/conv2a_bn_offset/Assign4^res_layer2/branch2/conv2a_bn/conv2a_bn_scale/Assign)^res_layer2/branch2/conv2b/weights/Assign5^res_layer2/branch2/conv2b_bn/conv2b_bn_offset/Assign4^res_layer2/branch2/conv2b_bn/conv2b_bn_scale/Assign)^res_layer2/branch2/conv2c/weights/Assign5^res_layer2/branch2/conv2c_bn/conv2c_bn_offset/Assign4^res_layer2/branch2/conv2c_bn/conv2c_bn_scale/Assign(^res_layer3/branch1/conv1/weights/Assign)^res_layer3/branch2/conv2a/weights/Assign5^res_layer3/branch2/conv2a_bn/conv2a_bn_offset/Assign4^res_layer3/branch2/conv2a_bn/conv2a_bn_scale/Assign)^res_layer3/branch2/conv2b/weights/Assign5^res_layer3/branch2/conv2b_bn/conv2b_bn_offset/Assign4^res_layer3/branch2/conv2b_bn/conv2b_bn_scale/Assign)^res_layer3/branch2/conv2c/weights/Assign5^res_layer3/branch2/conv2c_bn/conv2c_bn_offset/Assign4^res_layer3/branch2/conv2c_bn/conv2c_bn_scale/Assign^rnn_fnn_layer/bias/Assign ^rnn_fnn_layer/bias_class/Assign^rnn_fnn_layer/weights/Assign#^rnn_fnn_layer/weights_class/Assign"/device:CPU:0
&
initNoOp
^init/NoOp^init/NoOp_1

init_1NoOp
"

group_depsNoOp^init^init_1
�
4report_uninitialized_variables/IsVariableInitializedIsVariableInitialized res_layer1/branch1/conv1/weights"/device:CPU:0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_1IsVariableInitialized*res_layer1/branch1/conv1_bn/conv1_bn_scale"/device:CPU:0*
dtype0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale
�
6report_uninitialized_variables/IsVariableInitialized_2IsVariableInitialized+res_layer1/branch1/conv1_bn/conv1_bn_offset"/device:CPU:0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_3IsVariableInitialized!res_layer1/branch2/conv2a/weights"/device:CPU:0*
dtype0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights
�
6report_uninitialized_variables/IsVariableInitialized_4IsVariableInitialized,res_layer1/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_5IsVariableInitialized-res_layer1/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_6IsVariableInitialized!res_layer1/branch2/conv2b/weights"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_7IsVariableInitialized,res_layer1/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_8IsVariableInitialized-res_layer1/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
dtype0
�
6report_uninitialized_variables/IsVariableInitialized_9IsVariableInitialized!res_layer1/branch2/conv2c/weights"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_10IsVariableInitialized,res_layer1/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_11IsVariableInitialized-res_layer1/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_12IsVariableInitialized res_layer2/branch1/conv1/weights"/device:CPU:0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_13IsVariableInitialized!res_layer2/branch2/conv2a/weights"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_14IsVariableInitialized,res_layer2/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_15IsVariableInitialized-res_layer2/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_16IsVariableInitialized!res_layer2/branch2/conv2b/weights"/device:CPU:0*
dtype0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights
�
7report_uninitialized_variables/IsVariableInitialized_17IsVariableInitialized,res_layer2/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*
dtype0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale
�
7report_uninitialized_variables/IsVariableInitialized_18IsVariableInitialized-res_layer2/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset
�
7report_uninitialized_variables/IsVariableInitialized_19IsVariableInitialized!res_layer2/branch2/conv2c/weights"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_20IsVariableInitialized,res_layer2/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_21IsVariableInitialized-res_layer2/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset
�
7report_uninitialized_variables/IsVariableInitialized_22IsVariableInitialized res_layer3/branch1/conv1/weights"/device:CPU:0*
dtype0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights
�
7report_uninitialized_variables/IsVariableInitialized_23IsVariableInitialized!res_layer3/branch2/conv2a/weights"/device:CPU:0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_24IsVariableInitialized,res_layer3/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_25IsVariableInitialized-res_layer3/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_26IsVariableInitialized!res_layer3/branch2/conv2b/weights"/device:CPU:0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_27IsVariableInitialized,res_layer3/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_28IsVariableInitialized-res_layer3/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_29IsVariableInitialized!res_layer3/branch2/conv2c/weights"/device:CPU:0*
dtype0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights
�
7report_uninitialized_variables/IsVariableInitialized_30IsVariableInitialized,res_layer3/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_31IsVariableInitialized-res_layer3/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
�
7report_uninitialized_variables/IsVariableInitialized_32IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel
�
7report_uninitialized_variables/IsVariableInitialized_33IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_34IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_35IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_36IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_37IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_38IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_39IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
dtype0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias
�
7report_uninitialized_variables/IsVariableInitialized_40IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_41IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_42IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_43IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_44IsVariableInitializedrnn_fnn_layer/weights"/device:CPU:0*
dtype0*(
_class
loc:@rnn_fnn_layer/weights
�
7report_uninitialized_variables/IsVariableInitialized_45IsVariableInitializedrnn_fnn_layer/bias"/device:CPU:0*%
_class
loc:@rnn_fnn_layer/bias*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_46IsVariableInitializedrnn_fnn_layer/weights_class"/device:CPU:0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
dtype0
�
7report_uninitialized_variables/IsVariableInitialized_47IsVariableInitializedrnn_fnn_layer/bias_class"/device:CPU:0*
dtype0*+
_class!
loc:@rnn_fnn_layer/bias_class
�
$report_uninitialized_variables/stackPack4report_uninitialized_variables/IsVariableInitialized6report_uninitialized_variables/IsVariableInitialized_16report_uninitialized_variables/IsVariableInitialized_26report_uninitialized_variables/IsVariableInitialized_36report_uninitialized_variables/IsVariableInitialized_46report_uninitialized_variables/IsVariableInitialized_56report_uninitialized_variables/IsVariableInitialized_66report_uninitialized_variables/IsVariableInitialized_76report_uninitialized_variables/IsVariableInitialized_86report_uninitialized_variables/IsVariableInitialized_97report_uninitialized_variables/IsVariableInitialized_107report_uninitialized_variables/IsVariableInitialized_117report_uninitialized_variables/IsVariableInitialized_127report_uninitialized_variables/IsVariableInitialized_137report_uninitialized_variables/IsVariableInitialized_147report_uninitialized_variables/IsVariableInitialized_157report_uninitialized_variables/IsVariableInitialized_167report_uninitialized_variables/IsVariableInitialized_177report_uninitialized_variables/IsVariableInitialized_187report_uninitialized_variables/IsVariableInitialized_197report_uninitialized_variables/IsVariableInitialized_207report_uninitialized_variables/IsVariableInitialized_217report_uninitialized_variables/IsVariableInitialized_227report_uninitialized_variables/IsVariableInitialized_237report_uninitialized_variables/IsVariableInitialized_247report_uninitialized_variables/IsVariableInitialized_257report_uninitialized_variables/IsVariableInitialized_267report_uninitialized_variables/IsVariableInitialized_277report_uninitialized_variables/IsVariableInitialized_287report_uninitialized_variables/IsVariableInitialized_297report_uninitialized_variables/IsVariableInitialized_307report_uninitialized_variables/IsVariableInitialized_317report_uninitialized_variables/IsVariableInitialized_327report_uninitialized_variables/IsVariableInitialized_337report_uninitialized_variables/IsVariableInitialized_347report_uninitialized_variables/IsVariableInitialized_357report_uninitialized_variables/IsVariableInitialized_367report_uninitialized_variables/IsVariableInitialized_377report_uninitialized_variables/IsVariableInitialized_387report_uninitialized_variables/IsVariableInitialized_397report_uninitialized_variables/IsVariableInitialized_407report_uninitialized_variables/IsVariableInitialized_417report_uninitialized_variables/IsVariableInitialized_427report_uninitialized_variables/IsVariableInitialized_437report_uninitialized_variables/IsVariableInitialized_447report_uninitialized_variables/IsVariableInitialized_457report_uninitialized_variables/IsVariableInitialized_467report_uninitialized_variables/IsVariableInitialized_47"/device:CPU:0*
T0
*

axis *
N0
l
)report_uninitialized_variables/LogicalNot
LogicalNot$report_uninitialized_variables/stack"/device:CPU:0
�
$report_uninitialized_variables/ConstConst"/device:CPU:0*�
value�B�0B res_layer1/branch1/conv1/weightsB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB!res_layer1/branch2/conv2a/weightsB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer1/branch2/conv2b/weightsB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer1/branch2/conv2c/weightsB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer2/branch2/conv2b/weightsB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer2/branch2/conv2c/weightsB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer3/branch2/conv2b/weightsB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer3/branch2/conv2c/weightsB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasBrnn_fnn_layer/weightsBrnn_fnn_layer/biasBrnn_fnn_layer/weights_classBrnn_fnn_layer/bias_class*
dtype0
n
1report_uninitialized_variables/boolean_mask/ShapeConst"/device:CPU:0*
dtype0*
valueB:0
|
?report_uninitialized_variables/boolean_mask/strided_slice/stackConst"/device:CPU:0*
valueB: *
dtype0
~
Areport_uninitialized_variables/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0
~
Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
dtype0*
valueB:
�
9report_uninitialized_variables/boolean_mask/strided_sliceStridedSlice1report_uninitialized_variables/boolean_mask/Shape?report_uninitialized_variables/boolean_mask/strided_slice/stackAreport_uninitialized_variables/boolean_mask/strided_slice/stack_1Areport_uninitialized_variables/boolean_mask/strided_slice/stack_2"/device:CPU:0*
Index0*
T0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 

Breport_uninitialized_variables/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
dtype0*
valueB: 
�
0report_uninitialized_variables/boolean_mask/ProdProd9report_uninitialized_variables/boolean_mask/strided_sliceBreport_uninitialized_variables/boolean_mask/Prod/reduction_indices"/device:CPU:0*

Tidx0*
	keep_dims( *
T0
p
3report_uninitialized_variables/boolean_mask/Shape_1Const"/device:CPU:0*
valueB:0*
dtype0
~
Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
valueB: *
dtype0
�
Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
valueB: *
dtype0
�
Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
valueB:*
dtype0
�
;report_uninitialized_variables/boolean_mask/strided_slice_1StridedSlice3report_uninitialized_variables/boolean_mask/Shape_1Areport_uninitialized_variables/boolean_mask/strided_slice_1/stackCreport_uninitialized_variables/boolean_mask/strided_slice_1/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*
shrink_axis_mask *
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask *
Index0*
T0
p
3report_uninitialized_variables/boolean_mask/Shape_2Const"/device:CPU:0*
valueB:0*
dtype0
~
Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
valueB:*
dtype0
�
Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
dtype0*
valueB: 
�
Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
dtype0*
valueB:
�
;report_uninitialized_variables/boolean_mask/strided_slice_2StridedSlice3report_uninitialized_variables/boolean_mask/Shape_2Areport_uninitialized_variables/boolean_mask/strided_slice_2/stackCreport_uninitialized_variables/boolean_mask/strided_slice_2/stack_1Creport_uninitialized_variables/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
Index0*
T0
�
;report_uninitialized_variables/boolean_mask/concat/values_1Pack0report_uninitialized_variables/boolean_mask/Prod"/device:CPU:0*
N*
T0*

axis 
p
7report_uninitialized_variables/boolean_mask/concat/axisConst"/device:CPU:0*
value	B : *
dtype0
�
2report_uninitialized_variables/boolean_mask/concatConcatV2;report_uninitialized_variables/boolean_mask/strided_slice_1;report_uninitialized_variables/boolean_mask/concat/values_1;report_uninitialized_variables/boolean_mask/strided_slice_27report_uninitialized_variables/boolean_mask/concat/axis"/device:CPU:0*

Tidx0*
T0*
N
�
3report_uninitialized_variables/boolean_mask/ReshapeReshape$report_uninitialized_variables/Const2report_uninitialized_variables/boolean_mask/concat"/device:CPU:0*
T0*
Tshape0
�
;report_uninitialized_variables/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
dtype0*
valueB:
���������
�
5report_uninitialized_variables/boolean_mask/Reshape_1Reshape)report_uninitialized_variables/LogicalNot;report_uninitialized_variables/boolean_mask/Reshape_1/shape"/device:CPU:0*
T0
*
Tshape0
�
1report_uninitialized_variables/boolean_mask/WhereWhere5report_uninitialized_variables/boolean_mask/Reshape_1"/device:CPU:0*
T0

�
3report_uninitialized_variables/boolean_mask/SqueezeSqueeze1report_uninitialized_variables/boolean_mask/Where"/device:CPU:0*
squeeze_dims
*
T0	
r
9report_uninitialized_variables/boolean_mask/GatherV2/axisConst"/device:CPU:0*
dtype0*
value	B : 
�
4report_uninitialized_variables/boolean_mask/GatherV2GatherV23report_uninitialized_variables/boolean_mask/Reshape3report_uninitialized_variables/boolean_mask/Squeeze9report_uninitialized_variables/boolean_mask/GatherV2/axis"/device:CPU:0*
Taxis0*
Tindices0	*
Tparams0
\
$report_uninitialized_resources/ConstConst"/device:CPU:0*
valueB *
dtype0
5
concat/axisConst*
value	B : *
dtype0
�
concatConcatV24report_uninitialized_variables/boolean_mask/GatherV2$report_uninitialized_resources/Constconcat/axis*
T0*
N*

Tidx0
�
6report_uninitialized_variables_1/IsVariableInitializedIsVariableInitialized res_layer1/branch1/conv1/weights"/device:CPU:0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_1IsVariableInitialized*res_layer1/branch1/conv1_bn/conv1_bn_scale"/device:CPU:0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_2IsVariableInitialized+res_layer1/branch1/conv1_bn/conv1_bn_offset"/device:CPU:0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_3IsVariableInitialized!res_layer1/branch2/conv2a/weights"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_4IsVariableInitialized,res_layer1/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_5IsVariableInitialized-res_layer1/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset
�
8report_uninitialized_variables_1/IsVariableInitialized_6IsVariableInitialized!res_layer1/branch2/conv2b/weights"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_7IsVariableInitialized,res_layer1/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale*
dtype0
�
8report_uninitialized_variables_1/IsVariableInitialized_8IsVariableInitialized-res_layer1/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset
�
8report_uninitialized_variables_1/IsVariableInitialized_9IsVariableInitialized!res_layer1/branch2/conv2c/weights"/device:CPU:0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_10IsVariableInitialized,res_layer1/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_11IsVariableInitialized-res_layer1/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_12IsVariableInitialized res_layer2/branch1/conv1/weights"/device:CPU:0*
dtype0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights
�
9report_uninitialized_variables_1/IsVariableInitialized_13IsVariableInitialized!res_layer2/branch2/conv2a/weights"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_14IsVariableInitialized,res_layer2/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*
dtype0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale
�
9report_uninitialized_variables_1/IsVariableInitialized_15IsVariableInitialized-res_layer2/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_16IsVariableInitialized!res_layer2/branch2/conv2b/weights"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_17IsVariableInitialized,res_layer2/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_18IsVariableInitialized-res_layer2/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_19IsVariableInitialized!res_layer2/branch2/conv2c/weights"/device:CPU:0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_20IsVariableInitialized,res_layer2/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_21IsVariableInitialized-res_layer2/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_22IsVariableInitialized res_layer3/branch1/conv1/weights"/device:CPU:0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_23IsVariableInitialized!res_layer3/branch2/conv2a/weights"/device:CPU:0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_24IsVariableInitialized,res_layer3/branch2/conv2a_bn/conv2a_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_25IsVariableInitialized-res_layer3/branch2/conv2a_bn/conv2a_bn_offset"/device:CPU:0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_26IsVariableInitialized!res_layer3/branch2/conv2b/weights"/device:CPU:0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_27IsVariableInitialized,res_layer3/branch2/conv2b_bn/conv2b_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_28IsVariableInitialized-res_layer3/branch2/conv2b_bn/conv2b_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset
�
9report_uninitialized_variables_1/IsVariableInitialized_29IsVariableInitialized!res_layer3/branch2/conv2c/weights"/device:CPU:0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_30IsVariableInitialized,res_layer3/branch2/conv2c_bn/conv2c_bn_scale"/device:CPU:0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_31IsVariableInitialized-res_layer3/branch2/conv2c_bn/conv2c_bn_offset"/device:CPU:0*
dtype0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
�
9report_uninitialized_variables_1/IsVariableInitialized_32IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_33IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_34IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_35IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_36IsVariableInitialized3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_37IsVariableInitialized1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_38IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_39IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
dtype0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias
�
9report_uninitialized_variables_1/IsVariableInitialized_40IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_41IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_42IsVariableInitialized3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_43IsVariableInitialized1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_44IsVariableInitializedrnn_fnn_layer/weights"/device:CPU:0*(
_class
loc:@rnn_fnn_layer/weights*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_45IsVariableInitializedrnn_fnn_layer/bias"/device:CPU:0*%
_class
loc:@rnn_fnn_layer/bias*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_46IsVariableInitializedrnn_fnn_layer/weights_class"/device:CPU:0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
dtype0
�
9report_uninitialized_variables_1/IsVariableInitialized_47IsVariableInitializedrnn_fnn_layer/bias_class"/device:CPU:0*
dtype0*+
_class!
loc:@rnn_fnn_layer/bias_class
�
&report_uninitialized_variables_1/stackPack6report_uninitialized_variables_1/IsVariableInitialized8report_uninitialized_variables_1/IsVariableInitialized_18report_uninitialized_variables_1/IsVariableInitialized_28report_uninitialized_variables_1/IsVariableInitialized_38report_uninitialized_variables_1/IsVariableInitialized_48report_uninitialized_variables_1/IsVariableInitialized_58report_uninitialized_variables_1/IsVariableInitialized_68report_uninitialized_variables_1/IsVariableInitialized_78report_uninitialized_variables_1/IsVariableInitialized_88report_uninitialized_variables_1/IsVariableInitialized_99report_uninitialized_variables_1/IsVariableInitialized_109report_uninitialized_variables_1/IsVariableInitialized_119report_uninitialized_variables_1/IsVariableInitialized_129report_uninitialized_variables_1/IsVariableInitialized_139report_uninitialized_variables_1/IsVariableInitialized_149report_uninitialized_variables_1/IsVariableInitialized_159report_uninitialized_variables_1/IsVariableInitialized_169report_uninitialized_variables_1/IsVariableInitialized_179report_uninitialized_variables_1/IsVariableInitialized_189report_uninitialized_variables_1/IsVariableInitialized_199report_uninitialized_variables_1/IsVariableInitialized_209report_uninitialized_variables_1/IsVariableInitialized_219report_uninitialized_variables_1/IsVariableInitialized_229report_uninitialized_variables_1/IsVariableInitialized_239report_uninitialized_variables_1/IsVariableInitialized_249report_uninitialized_variables_1/IsVariableInitialized_259report_uninitialized_variables_1/IsVariableInitialized_269report_uninitialized_variables_1/IsVariableInitialized_279report_uninitialized_variables_1/IsVariableInitialized_289report_uninitialized_variables_1/IsVariableInitialized_299report_uninitialized_variables_1/IsVariableInitialized_309report_uninitialized_variables_1/IsVariableInitialized_319report_uninitialized_variables_1/IsVariableInitialized_329report_uninitialized_variables_1/IsVariableInitialized_339report_uninitialized_variables_1/IsVariableInitialized_349report_uninitialized_variables_1/IsVariableInitialized_359report_uninitialized_variables_1/IsVariableInitialized_369report_uninitialized_variables_1/IsVariableInitialized_379report_uninitialized_variables_1/IsVariableInitialized_389report_uninitialized_variables_1/IsVariableInitialized_399report_uninitialized_variables_1/IsVariableInitialized_409report_uninitialized_variables_1/IsVariableInitialized_419report_uninitialized_variables_1/IsVariableInitialized_429report_uninitialized_variables_1/IsVariableInitialized_439report_uninitialized_variables_1/IsVariableInitialized_449report_uninitialized_variables_1/IsVariableInitialized_459report_uninitialized_variables_1/IsVariableInitialized_469report_uninitialized_variables_1/IsVariableInitialized_47"/device:CPU:0*
T0
*

axis *
N0
p
+report_uninitialized_variables_1/LogicalNot
LogicalNot&report_uninitialized_variables_1/stack"/device:CPU:0
�
&report_uninitialized_variables_1/ConstConst"/device:CPU:0*
dtype0*�
value�B�0B res_layer1/branch1/conv1/weightsB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB!res_layer1/branch2/conv2a/weightsB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer1/branch2/conv2b/weightsB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer1/branch2/conv2c/weightsB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer2/branch2/conv2b/weightsB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer2/branch2/conv2c/weightsB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB!res_layer3/branch2/conv2b/weightsB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB!res_layer3/branch2/conv2c/weightsB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasBrnn_fnn_layer/weightsBrnn_fnn_layer/biasBrnn_fnn_layer/weights_classBrnn_fnn_layer/bias_class
p
3report_uninitialized_variables_1/boolean_mask/ShapeConst"/device:CPU:0*
valueB:0*
dtype0
~
Areport_uninitialized_variables_1/boolean_mask/strided_slice/stackConst"/device:CPU:0*
valueB: *
dtype0
�
Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Const"/device:CPU:0*
valueB:*
dtype0
�
Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2Const"/device:CPU:0*
valueB:*
dtype0
�
;report_uninitialized_variables_1/boolean_mask/strided_sliceStridedSlice3report_uninitialized_variables_1/boolean_mask/ShapeAreport_uninitialized_variables_1/boolean_mask/strided_slice/stackCreport_uninitialized_variables_1/boolean_mask/strided_slice/stack_1Creport_uninitialized_variables_1/boolean_mask/strided_slice/stack_2"/device:CPU:0*
Index0*
T0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask 
�
Dreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indicesConst"/device:CPU:0*
valueB: *
dtype0
�
2report_uninitialized_variables_1/boolean_mask/ProdProd;report_uninitialized_variables_1/boolean_mask/strided_sliceDreport_uninitialized_variables_1/boolean_mask/Prod/reduction_indices"/device:CPU:0*

Tidx0*
	keep_dims( *
T0
r
5report_uninitialized_variables_1/boolean_mask/Shape_1Const"/device:CPU:0*
valueB:0*
dtype0
�
Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackConst"/device:CPU:0*
valueB: *
dtype0
�
Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Const"/device:CPU:0*
valueB: *
dtype0
�
Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2Const"/device:CPU:0*
valueB:*
dtype0
�
=report_uninitialized_variables_1/boolean_mask/strided_slice_1StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_1Creport_uninitialized_variables_1/boolean_mask/strided_slice_1/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_1/stack_2"/device:CPU:0*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask *
Index0*
T0*
shrink_axis_mask 
r
5report_uninitialized_variables_1/boolean_mask/Shape_2Const"/device:CPU:0*
valueB:0*
dtype0
�
Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackConst"/device:CPU:0*
valueB:*
dtype0
�
Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Const"/device:CPU:0*
dtype0*
valueB: 
�
Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2Const"/device:CPU:0*
valueB:*
dtype0
�
=report_uninitialized_variables_1/boolean_mask/strided_slice_2StridedSlice5report_uninitialized_variables_1/boolean_mask/Shape_2Creport_uninitialized_variables_1/boolean_mask/strided_slice_2/stackEreport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_1Ereport_uninitialized_variables_1/boolean_mask/strided_slice_2/stack_2"/device:CPU:0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
T0*
Index0
�
=report_uninitialized_variables_1/boolean_mask/concat/values_1Pack2report_uninitialized_variables_1/boolean_mask/Prod"/device:CPU:0*
N*
T0*

axis 
r
9report_uninitialized_variables_1/boolean_mask/concat/axisConst"/device:CPU:0*
value	B : *
dtype0
�
4report_uninitialized_variables_1/boolean_mask/concatConcatV2=report_uninitialized_variables_1/boolean_mask/strided_slice_1=report_uninitialized_variables_1/boolean_mask/concat/values_1=report_uninitialized_variables_1/boolean_mask/strided_slice_29report_uninitialized_variables_1/boolean_mask/concat/axis"/device:CPU:0*
N*

Tidx0*
T0
�
5report_uninitialized_variables_1/boolean_mask/ReshapeReshape&report_uninitialized_variables_1/Const4report_uninitialized_variables_1/boolean_mask/concat"/device:CPU:0*
T0*
Tshape0
�
=report_uninitialized_variables_1/boolean_mask/Reshape_1/shapeConst"/device:CPU:0*
valueB:
���������*
dtype0
�
7report_uninitialized_variables_1/boolean_mask/Reshape_1Reshape+report_uninitialized_variables_1/LogicalNot=report_uninitialized_variables_1/boolean_mask/Reshape_1/shape"/device:CPU:0*
T0
*
Tshape0
�
3report_uninitialized_variables_1/boolean_mask/WhereWhere7report_uninitialized_variables_1/boolean_mask/Reshape_1"/device:CPU:0*
T0

�
5report_uninitialized_variables_1/boolean_mask/SqueezeSqueeze3report_uninitialized_variables_1/boolean_mask/Where"/device:CPU:0*
squeeze_dims
*
T0	
t
;report_uninitialized_variables_1/boolean_mask/GatherV2/axisConst"/device:CPU:0*
dtype0*
value	B : 
�
6report_uninitialized_variables_1/boolean_mask/GatherV2GatherV25report_uninitialized_variables_1/boolean_mask/Reshape5report_uninitialized_variables_1/boolean_mask/Squeeze;report_uninitialized_variables_1/boolean_mask/GatherV2/axis"/device:CPU:0*
Taxis0*
Tindices0	*
Tparams0
^
&report_uninitialized_resources_1/ConstConst"/device:CPU:0*
valueB *
dtype0
7
concat_1/axisConst*
value	B : *
dtype0
�
concat_1ConcatV26report_uninitialized_variables_1/boolean_mask/GatherV2&report_uninitialized_resources_1/Constconcat_1/axis*

Tidx0*
T0*
N

init_2NoOp

init_all_tablesNoOp

init_3NoOp
8
group_deps_1NoOp^init_2^init_3^init_all_tables
C
save_1/filename/inputConst*
valueB Bmodel*
dtype0
Z
save_1/filenamePlaceholderWithDefaultsave_1/filename/input*
shape: *
dtype0
Q
save_1/ConstPlaceholderWithDefaultsave_1/filename*
dtype0*
shape: 
n
save_1/StringJoin/inputs_1Const*<
value3B1 B+_temp_bc6648f1c6f341958bbdac82296c7ac2/part*
dtype0
c
save_1/StringJoin
StringJoinsave_1/Constsave_1/StringJoin/inputs_1*
N*
	separator 
;
save_1/num_shardsConst*
dtype0*
value	B :
U
save_1/ShardedFilename/shardConst"/device:CPU:0*
value	B : *
dtype0
|
save_1/ShardedFilenameShardedFilenamesave_1/StringJoinsave_1/ShardedFilename/shardsave_1/num_shards"/device:CPU:0
�
save_1/SaveV2/tensor_namesConst"/device:CPU:0*�
value�B�B1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
dtype0
p
save_1/SaveV2/shape_and_slicesConst"/device:CPU:0*+
value"B B B B B B B B B B B B B *
dtype0
�
save_1/SaveV2SaveV2save_1/ShardedFilenamesave_1/SaveV2/tensor_namessave_1/SaveV2/shape_and_slices1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel"/device:CPU:0*
dtypes
2
�
save_1/control_dependencyIdentitysave_1/ShardedFilename^save_1/SaveV2"/device:CPU:0*
T0*)
_class
loc:@save_1/ShardedFilename
W
save_1/ShardedFilename_1/shardConst"/device:CPU:0*
value	B :*
dtype0
�
save_1/ShardedFilename_1ShardedFilenamesave_1/StringJoinsave_1/ShardedFilename_1/shardsave_1/num_shards"/device:CPU:0
�
save_1/SaveV2_1/tensor_namesConst"/device:CPU:0*�
value�B�$B res_layer1/branch1/conv1/weightsB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB!res_layer1/branch2/conv2a/weightsB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer1/branch2/conv2b/weightsB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer1/branch2/conv2c/weightsB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer2/branch2/conv2b/weightsB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer2/branch2/conv2c/weightsB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer3/branch2/conv2b/weightsB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer3/branch2/conv2c/weightsB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleBrnn_fnn_layer/biasBrnn_fnn_layer/bias_classBrnn_fnn_layer/weightsBrnn_fnn_layer/weights_class*
dtype0
�
 save_1/SaveV2_1/shape_and_slicesConst"/device:CPU:0*[
valueRBP$B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0
�
save_1/SaveV2_1SaveV2save_1/ShardedFilename_1save_1/SaveV2_1/tensor_names save_1/SaveV2_1/shape_and_slices res_layer1/branch1/conv1/weights+res_layer1/branch1/conv1_bn/conv1_bn_offset*res_layer1/branch1/conv1_bn/conv1_bn_scale!res_layer1/branch2/conv2a/weights-res_layer1/branch2/conv2a_bn/conv2a_bn_offset,res_layer1/branch2/conv2a_bn/conv2a_bn_scale!res_layer1/branch2/conv2b/weights-res_layer1/branch2/conv2b_bn/conv2b_bn_offset,res_layer1/branch2/conv2b_bn/conv2b_bn_scale!res_layer1/branch2/conv2c/weights-res_layer1/branch2/conv2c_bn/conv2c_bn_offset,res_layer1/branch2/conv2c_bn/conv2c_bn_scale res_layer2/branch1/conv1/weights!res_layer2/branch2/conv2a/weights-res_layer2/branch2/conv2a_bn/conv2a_bn_offset,res_layer2/branch2/conv2a_bn/conv2a_bn_scale!res_layer2/branch2/conv2b/weights-res_layer2/branch2/conv2b_bn/conv2b_bn_offset,res_layer2/branch2/conv2b_bn/conv2b_bn_scale!res_layer2/branch2/conv2c/weights-res_layer2/branch2/conv2c_bn/conv2c_bn_offset,res_layer2/branch2/conv2c_bn/conv2c_bn_scale res_layer3/branch1/conv1/weights!res_layer3/branch2/conv2a/weights-res_layer3/branch2/conv2a_bn/conv2a_bn_offset,res_layer3/branch2/conv2a_bn/conv2a_bn_scale!res_layer3/branch2/conv2b/weights-res_layer3/branch2/conv2b_bn/conv2b_bn_offset,res_layer3/branch2/conv2b_bn/conv2b_bn_scale!res_layer3/branch2/conv2c/weights-res_layer3/branch2/conv2c_bn/conv2c_bn_offset,res_layer3/branch2/conv2c_bn/conv2c_bn_scalernn_fnn_layer/biasrnn_fnn_layer/bias_classrnn_fnn_layer/weightsrnn_fnn_layer/weights_class"/device:CPU:0*2
dtypes(
&2$
�
save_1/control_dependency_1Identitysave_1/ShardedFilename_1^save_1/SaveV2_1"/device:CPU:0*
T0*+
_class!
loc:@save_1/ShardedFilename_1
�
-save_1/MergeV2Checkpoints/checkpoint_prefixesPacksave_1/ShardedFilenamesave_1/ShardedFilename_1^save_1/control_dependency^save_1/control_dependency_1"/device:CPU:0*
N*
T0*

axis 
�
save_1/MergeV2CheckpointsMergeV2Checkpoints-save_1/MergeV2Checkpoints/checkpoint_prefixessave_1/Const"/device:CPU:0*
delete_old_dirs(
�
save_1/IdentityIdentitysave_1/Const^save_1/MergeV2Checkpoints^save_1/control_dependency^save_1/control_dependency_1"/device:CPU:0*
T0
�
save_1/RestoreV2/tensor_namesConst"/device:CPU:0*
dtype0*�
value�B�B1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelB1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biasB3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel
s
!save_1/RestoreV2/shape_and_slicesConst"/device:CPU:0*+
value"B B B B B B B B B B B B B *
dtype0
�
save_1/RestoreV2	RestoreV2save_1/Constsave_1/RestoreV2/tensor_names!save_1/RestoreV2/shape_and_slices"/device:CPU:0*
dtypes
2
�
save_1/AssignAssign1BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/biassave_1/RestoreV2*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/bias*
validate_shape(
�
save_1/Assign_1Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernelsave_1/RestoreV2:1*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_0/lstm_cell/kernel*
validate_shape(*
use_locking(
�
save_1/Assign_2Assign1BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/biassave_1/RestoreV2:2*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(
�
save_1/Assign_3Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernelsave_1/RestoreV2:3*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_1/lstm_cell/kernel*
validate_shape(
�
save_1/Assign_4Assign1BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/biassave_1/RestoreV2:4*
T0*D
_class:
86loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/bias*
validate_shape(*
use_locking(
�
save_1/Assign_5Assign3BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernelsave_1/RestoreV2:5*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/bw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(
�
save_1/Assign_6Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/biassave_1/RestoreV2:6*
validate_shape(*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/bias
�
save_1/Assign_7Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernelsave_1/RestoreV2:7*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_0/lstm_cell/kernel*
validate_shape(*
use_locking(
�
save_1/Assign_8Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/biassave_1/RestoreV2:8*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/bias*
validate_shape(
�
save_1/Assign_9Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernelsave_1/RestoreV2:9*
use_locking(*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_1/lstm_cell/kernel*
validate_shape(
�
save_1/Assign_10Assign1BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/biassave_1/RestoreV2:10*
validate_shape(*
use_locking(*
T0*D
_class:
86loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/bias
�
save_1/Assign_11Assign3BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernelsave_1/RestoreV2:11*
T0*F
_class<
:8loc:@BDGRU_rnn/fw/multi_rnn_cell/cell_2/lstm_cell/kernel*
validate_shape(*
use_locking(
�
save_1/restore_shardNoOp^save_1/Assign^save_1/Assign_1^save_1/Assign_10^save_1/Assign_11^save_1/Assign_2^save_1/Assign_3^save_1/Assign_4^save_1/Assign_5^save_1/Assign_6^save_1/Assign_7^save_1/Assign_8^save_1/Assign_9
�
save_1/RestoreV2_1/tensor_namesConst"/device:CPU:0*�
value�B�$B res_layer1/branch1/conv1/weightsB+res_layer1/branch1/conv1_bn/conv1_bn_offsetB*res_layer1/branch1/conv1_bn/conv1_bn_scaleB!res_layer1/branch2/conv2a/weightsB-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer1/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer1/branch2/conv2b/weightsB-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer1/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer1/branch2/conv2c/weightsB-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer1/branch2/conv2c_bn/conv2c_bn_scaleB res_layer2/branch1/conv1/weightsB!res_layer2/branch2/conv2a/weightsB-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer2/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer2/branch2/conv2b/weightsB-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer2/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer2/branch2/conv2c/weightsB-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer2/branch2/conv2c_bn/conv2c_bn_scaleB res_layer3/branch1/conv1/weightsB!res_layer3/branch2/conv2a/weightsB-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetB,res_layer3/branch2/conv2a_bn/conv2a_bn_scaleB!res_layer3/branch2/conv2b/weightsB-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetB,res_layer3/branch2/conv2b_bn/conv2b_bn_scaleB!res_layer3/branch2/conv2c/weightsB-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetB,res_layer3/branch2/conv2c_bn/conv2c_bn_scaleBrnn_fnn_layer/biasBrnn_fnn_layer/bias_classBrnn_fnn_layer/weightsBrnn_fnn_layer/weights_class*
dtype0
�
#save_1/RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
dtype0*[
valueRBP$B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
�
save_1/RestoreV2_1	RestoreV2save_1/Constsave_1/RestoreV2_1/tensor_names#save_1/RestoreV2_1/shape_and_slices"/device:CPU:0*2
dtypes(
&2$
�
save_1/Assign_12Assign res_layer1/branch1/conv1/weightssave_1/RestoreV2_1"/device:CPU:0*
validate_shape(*
use_locking(*
T0*3
_class)
'%loc:@res_layer1/branch1/conv1/weights
�
save_1/Assign_13Assign+res_layer1/branch1/conv1_bn/conv1_bn_offsetsave_1/RestoreV2_1:1"/device:CPU:0*
validate_shape(*
use_locking(*
T0*>
_class4
20loc:@res_layer1/branch1/conv1_bn/conv1_bn_offset
�
save_1/Assign_14Assign*res_layer1/branch1/conv1_bn/conv1_bn_scalesave_1/RestoreV2_1:2"/device:CPU:0*
validate_shape(*
use_locking(*
T0*=
_class3
1/loc:@res_layer1/branch1/conv1_bn/conv1_bn_scale
�
save_1/Assign_15Assign!res_layer1/branch2/conv2a/weightssave_1/RestoreV2_1:3"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2a/weights*
validate_shape(
�
save_1/Assign_16Assign-res_layer1/branch2/conv2a_bn/conv2a_bn_offsetsave_1/RestoreV2_1:4"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(
�
save_1/Assign_17Assign,res_layer1/branch2/conv2a_bn/conv2a_bn_scalesave_1/RestoreV2_1:5"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(
�
save_1/Assign_18Assign!res_layer1/branch2/conv2b/weightssave_1/RestoreV2_1:6"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2b/weights
�
save_1/Assign_19Assign-res_layer1/branch2/conv2b_bn/conv2b_bn_offsetsave_1/RestoreV2_1:7"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
save_1/Assign_20Assign,res_layer1/branch2/conv2b_bn/conv2b_bn_scalesave_1/RestoreV2_1:8"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2b_bn/conv2b_bn_scale
�
save_1/Assign_21Assign!res_layer1/branch2/conv2c/weightssave_1/RestoreV2_1:9"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer1/branch2/conv2c/weights*
validate_shape(*
use_locking(
�
save_1/Assign_22Assign-res_layer1/branch2/conv2c_bn/conv2c_bn_offsetsave_1/RestoreV2_1:10"/device:CPU:0*
T0*@
_class6
42loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(*
use_locking(
�
save_1/Assign_23Assign,res_layer1/branch2/conv2c_bn/conv2c_bn_scalesave_1/RestoreV2_1:11"/device:CPU:0*
validate_shape(*
use_locking(*
T0*?
_class5
31loc:@res_layer1/branch2/conv2c_bn/conv2c_bn_scale
�
save_1/Assign_24Assign res_layer2/branch1/conv1/weightssave_1/RestoreV2_1:12"/device:CPU:0*
use_locking(*
T0*3
_class)
'%loc:@res_layer2/branch1/conv1/weights*
validate_shape(
�
save_1/Assign_25Assign!res_layer2/branch2/conv2a/weightssave_1/RestoreV2_1:13"/device:CPU:0*
validate_shape(*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2a/weights
�
save_1/Assign_26Assign-res_layer2/branch2/conv2a_bn/conv2a_bn_offsetsave_1/RestoreV2_1:14"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(*
use_locking(
�
save_1/Assign_27Assign,res_layer2/branch2/conv2a_bn/conv2a_bn_scalesave_1/RestoreV2_1:15"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer2/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(
�
save_1/Assign_28Assign!res_layer2/branch2/conv2b/weightssave_1/RestoreV2_1:16"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2b/weights*
validate_shape(
�
save_1/Assign_29Assign-res_layer2/branch2/conv2b_bn/conv2b_bn_offsetsave_1/RestoreV2_1:17"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
save_1/Assign_30Assign,res_layer2/branch2/conv2b_bn/conv2b_bn_scalesave_1/RestoreV2_1:18"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2b_bn/conv2b_bn_scale*
validate_shape(*
use_locking(
�
save_1/Assign_31Assign!res_layer2/branch2/conv2c/weightssave_1/RestoreV2_1:19"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer2/branch2/conv2c/weights*
validate_shape(*
use_locking(
�
save_1/Assign_32Assign-res_layer2/branch2/conv2c_bn/conv2c_bn_offsetsave_1/RestoreV2_1:20"/device:CPU:0*
T0*@
_class6
42loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_offset*
validate_shape(*
use_locking(
�
save_1/Assign_33Assign,res_layer2/branch2/conv2c_bn/conv2c_bn_scalesave_1/RestoreV2_1:21"/device:CPU:0*
T0*?
_class5
31loc:@res_layer2/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(*
use_locking(
�
save_1/Assign_34Assign res_layer3/branch1/conv1/weightssave_1/RestoreV2_1:22"/device:CPU:0*
validate_shape(*
use_locking(*
T0*3
_class)
'%loc:@res_layer3/branch1/conv1/weights
�
save_1/Assign_35Assign!res_layer3/branch2/conv2a/weightssave_1/RestoreV2_1:23"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2a/weights*
validate_shape(*
use_locking(
�
save_1/Assign_36Assign-res_layer3/branch2/conv2a_bn/conv2a_bn_offsetsave_1/RestoreV2_1:24"/device:CPU:0*
T0*@
_class6
42loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_offset*
validate_shape(*
use_locking(
�
save_1/Assign_37Assign,res_layer3/branch2/conv2a_bn/conv2a_bn_scalesave_1/RestoreV2_1:25"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2a_bn/conv2a_bn_scale*
validate_shape(
�
save_1/Assign_38Assign!res_layer3/branch2/conv2b/weightssave_1/RestoreV2_1:26"/device:CPU:0*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2b/weights*
validate_shape(*
use_locking(
�
save_1/Assign_39Assign-res_layer3/branch2/conv2b_bn/conv2b_bn_offsetsave_1/RestoreV2_1:27"/device:CPU:0*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_offset*
validate_shape(
�
save_1/Assign_40Assign,res_layer3/branch2/conv2b_bn/conv2b_bn_scalesave_1/RestoreV2_1:28"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2b_bn/conv2b_bn_scale*
validate_shape(
�
save_1/Assign_41Assign!res_layer3/branch2/conv2c/weightssave_1/RestoreV2_1:29"/device:CPU:0*
use_locking(*
T0*4
_class*
(&loc:@res_layer3/branch2/conv2c/weights*
validate_shape(
�
save_1/Assign_42Assign-res_layer3/branch2/conv2c_bn/conv2c_bn_offsetsave_1/RestoreV2_1:30"/device:CPU:0*
validate_shape(*
use_locking(*
T0*@
_class6
42loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_offset
�
save_1/Assign_43Assign,res_layer3/branch2/conv2c_bn/conv2c_bn_scalesave_1/RestoreV2_1:31"/device:CPU:0*
use_locking(*
T0*?
_class5
31loc:@res_layer3/branch2/conv2c_bn/conv2c_bn_scale*
validate_shape(
�
save_1/Assign_44Assignrnn_fnn_layer/biassave_1/RestoreV2_1:32"/device:CPU:0*
use_locking(*
T0*%
_class
loc:@rnn_fnn_layer/bias*
validate_shape(
�
save_1/Assign_45Assignrnn_fnn_layer/bias_classsave_1/RestoreV2_1:33"/device:CPU:0*
use_locking(*
T0*+
_class!
loc:@rnn_fnn_layer/bias_class*
validate_shape(
�
save_1/Assign_46Assignrnn_fnn_layer/weightssave_1/RestoreV2_1:34"/device:CPU:0*
use_locking(*
T0*(
_class
loc:@rnn_fnn_layer/weights*
validate_shape(
�
save_1/Assign_47Assignrnn_fnn_layer/weights_classsave_1/RestoreV2_1:35"/device:CPU:0*
use_locking(*
T0*.
_class$
" loc:@rnn_fnn_layer/weights_class*
validate_shape(
�
save_1/restore_shard_1NoOp^save_1/Assign_12^save_1/Assign_13^save_1/Assign_14^save_1/Assign_15^save_1/Assign_16^save_1/Assign_17^save_1/Assign_18^save_1/Assign_19^save_1/Assign_20^save_1/Assign_21^save_1/Assign_22^save_1/Assign_23^save_1/Assign_24^save_1/Assign_25^save_1/Assign_26^save_1/Assign_27^save_1/Assign_28^save_1/Assign_29^save_1/Assign_30^save_1/Assign_31^save_1/Assign_32^save_1/Assign_33^save_1/Assign_34^save_1/Assign_35^save_1/Assign_36^save_1/Assign_37^save_1/Assign_38^save_1/Assign_39^save_1/Assign_40^save_1/Assign_41^save_1/Assign_42^save_1/Assign_43^save_1/Assign_44^save_1/Assign_45^save_1/Assign_46^save_1/Assign_47"/device:CPU:0
6
save_1/restore_all/NoOpNoOp^save_1/restore_shard
I
save_1/restore_all/NoOp_1NoOp^save_1/restore_shard_1"/device:CPU:0
P
save_1/restore_allNoOp^save_1/restore_all/NoOp^save_1/restore_all/NoOp_1"