import tensorflow as tf
from tensorflow.python.client import timeline

sess = tf.InteractiveSession() 
run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()

# create some dummy Ops for the graph
C1 = tf.constant(5)
C2 = tf.constant(3)
myOp = C1*C2 + tf.square(C2)
res = sess.run([myOp], options=run_options,run_metadata = run_metadata)
writer = tf.summary.FileWriter(logdir='tensorboard/profile_bug',graph=sess.graph)
print (res)

tl = timeline.Timeline(run_metadata.step_stats)
ctf = tl.generate_chrome_trace_format()
with open('tensorboard/timelineOfBug.json', 'w') as f:
    f.write(ctf)

writer.add_run_metadata(run_metadata,"mySess")
writer.close()
sess.close()

